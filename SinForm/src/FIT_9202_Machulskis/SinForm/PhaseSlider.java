package FIT_9202_Machulskis.SinForm;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public
class PhaseSlider extends JSlider
{
    PhaseSlider(final JSpinner phaseSpinner, int minimum, int maximum, int value)
    {
        super(minimum, maximum);
        setValue(value);
        this.addChangeListener(new ChangeListener()
        {
            @Override
            public
            void stateChanged(ChangeEvent e)
            {
                {
                    JSlider slider = (JSlider) (e.getSource());
                    if(slider instanceof PhaseSlider)
                    {
                        PhaseSpinnerEditor spe = (PhaseSpinnerEditor) phaseSpinner.getEditor();
                        spe.updateValue(slider.getValue());
                    }
                }
            }
        });

    }
}
