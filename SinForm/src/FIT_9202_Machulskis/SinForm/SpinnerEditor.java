package FIT_9202_Machulskis.SinForm;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class SpinnerEditor extends JSpinner.NumberEditor
{
    SinusPane pane;
    final int SPINNER_WIDTH = 30;
    final int SPINNER_HEIGHT = 30;
    final int DELTA_T = 1000;
    JSpinner spinner;
    String lastString ="";


    public SpinnerEditor(final JSpinner spinner, SinusPane pane)
    {
      super(spinner, "###");
      this.setOpaque(true);
      getTextField().setEditable(true);
      getTextField().setFocusLostBehavior(JFormattedTextField.COMMIT);
      getTextField().addFocusListener(new FocusListener()
      {
          @Override
          public void focusGained(FocusEvent focusEvent)
          {
          }

          @Override
          public void focusLost(FocusEvent focusEvent)
          {
                    FormattedTextFieldVerifier fv = new FormattedTextFieldVerifier();
                    boolean check = fv.verify(SpinnerEditor.this.getTextField());
                    if(!check)
                    {
                       SpinnerEditor.this.getTextField().requestFocus();
                       SpinnerEditor.this.getTextField().setText(lastString);
                       showTip();
                    }

          }
      });

        ControlModel model = (ControlModel)spinner.getModel();
        spinner.setToolTipText("Please type number between " + model.getMin() + " and "+ model.getMax());

        getTextField().addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyTyped(KeyEvent e)
            {
                JFormattedTextField format = getTextField();
                if(e.getKeyCode()==KeyEvent.VK_ENTER)
                {

                    commitEdit();
                }
                else
                {

                    FormattedTextFieldVerifier fv = new FormattedTextFieldVerifier();
                    boolean check = fv.verify(format);
                    if(!check)
                    {
                        lastString = format.getText();
                        showTip();


                    }
                }
            }
        });
        Dimension size = new Dimension(SPINNER_WIDTH, SPINNER_HEIGHT);
        setMinimumSize(size);
        setPreferredSize(size);
        this.spinner = spinner;

        this.pane = pane;
    }
    private void showTip()
    {
        PopupFactory popupFactory = PopupFactory.getSharedInstance();
        int x = spinner.getLocationOnScreen().x;
        int y = spinner.getLocationOnScreen().y;
        x += spinner.getWidth() / 2;
        y += spinner.getHeight();
        JToolTip tip =spinner.createToolTip();
        tip.setTipText(spinner.getToolTipText());
        final Popup tooltipContainer = popupFactory.getPopup(spinner,tip , x, y);
        tooltipContainer.show();
        (new Thread(new Runnable()
        {
            public void run()
            {
                try
                {
                    Thread.sleep(DELTA_T);
                }
                catch(InterruptedException ex)
                {

                }
                tooltipContainer.hide();
            }

        })).start();
    }
    protected void updateValue(JSpinner spinner)
    {
    }

    @Override
    public void commitEdit()
    {
        JFormattedTextField format = getTextField();
        FormattedTextFieldVerifier fv = new FormattedTextFieldVerifier();
        boolean check = fv.verify(format);

        if(check==true)
        {
            updateValue(spinner);
        }
        else
        {
            lastString = format.getText();
            try
            {
               spinner.setValue(spinner.getPreviousValue());
            }
            catch (IllegalArgumentException ex)
            {
            }
        }
    }

    @Override
    public void stateChanged(ChangeEvent e)
    {
        super.stateChanged(e);
        updateValue((JSpinner) e.getSource());

    }
}
