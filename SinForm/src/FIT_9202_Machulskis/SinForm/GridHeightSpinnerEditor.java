package FIT_9202_Machulskis.SinForm;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.text.NumberFormatter;
import java.text.Format;


public
class GridHeightSpinnerEditor extends SpinnerEditor
{
    JSlider slider;
    JSpinner sp;

    public
    GridHeightSpinnerEditor(JSpinner spinner, SinusPane pane)
    {
        super(spinner, pane);
        spinner.addChangeListener(this);
        sp = spinner;
        JFormattedTextField field = new JFormattedTextField(new NumberFormatter());
        JFormattedTextField f = getTextField();
        f= field;

    }

    @Override
    protected
    void updateValue(JSpinner spinner)
    {
        if(spinner.getEditor() instanceof GridHeightSpinnerEditor)
        {
            ControlModel model = (ControlModel) spinner.getModel();
            Integer value = (Integer) spinner.getValue();

            if((Integer) model.getMaximum() < value)
            {
                spinner.setValue(model.getMaximum());
                slider.setValue((Integer) model.getMaximum());
            }
            else
            {
                pane.changeHeight(value);
                spinner.setValue(value);
                slider.setValue((Integer) value);
            }
        }
    }

    @Override
    public
    void stateChanged(ChangeEvent e)
    {
        super.stateChanged(e);
        JSpinner mySpinner = (JSpinner) (e.getSource());
        updateValue(mySpinner);
    }

    protected
    void addSliderListener(JSlider slider)
    {
        this.slider = slider;
    }

    protected
    void updateValue(int value)
    {
        sp.setValue(value);
    }
}
