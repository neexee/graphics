package FIT_9202_Machulskis.SinForm;

import javax.swing.*;


/**
 * User: violetta
 * Date: 2/19/12
 * Time: 11:00 PM
 */
public
class ControlModel extends SpinnerNumberModel
{
    int value;
    int step;
    int maximum;
    int minimum;


    ControlModel(int minumum, int maximum, int value, int step)
    {
        super(value, minumum, maximum, step);
        this.minimum = minumum;
        this.maximum = maximum;
        this.value = value;
        this.step = step;

    }
    int getMax()
    {
        return maximum;
    }
    int getMin()
    {
        return minimum;
    }



}
