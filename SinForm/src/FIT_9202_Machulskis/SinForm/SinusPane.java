package FIT_9202_Machulskis.SinForm;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public
class SinusPane extends JPanel
{

    public final static int DEFAULT_PHASE = 100;
    public final static int DEFAULT_AMPLITUDE = 20;
    public final static int DEFAULT_GRID_WIDTH = 20;
    public final static int DEFAULT_GRID_HEIGHT = 20;

    public final static int MIN_PHASE = 1;
    public final static int MAX_PHASE = 100;

    public final static int MIN_AMPLITUDE = -50;
    public final static int MAX_AMPLITUDE = 50;

    public final static int MIN_GRID_WIDTH = 1;
    public final static int MAX_GRID_WIDTH = 100;

    public final static int MIN_GRID_HEIGHT = 1;
    public final static int MAX_GRID_HEIGHT = 100;
    
    public final static int MINIMAL_SIZE = 0;
    
    private final static int GRAPH_COLOR =200;

    BufferedImage image;

    int width = 0;
    int height = 0;
    int phase = DEFAULT_PHASE;
    int amplitude = DEFAULT_AMPLITUDE;
    int gridWidth = DEFAULT_GRID_WIDTH;
    int gridHeight = DEFAULT_GRID_HEIGHT;

    SinusPane(int phase, int amplitude, int gridWidth, int gridHeight)
    {
        this.amplitude = amplitude;
        this.phase = phase;
        this.gridHeight = gridHeight;
        this.gridWidth = gridWidth;
    }
    public void changeWidth(int newWidth)
    {
        gridWidth = newWidth;
        repaint();
    }

    public void changeHeight(int newHeight)
    {
        gridHeight = newHeight;
        repaint();
    }

    public void changePhase(int newPhase)
    {
        phase = newPhase;
        repaint();
    }

    public void changeAmplitude(int newAmplitude)
    {
        amplitude = newAmplitude;
        repaint();
    }

    @Override
    public void paintComponent(Graphics g)
    {
        this.width = getSize().width;
        this.height = getSize().height;
        int NUMBER_OF_PIXELS = width*height;

        Color color = Color.GREEN;

        int green = color.getRGB();
        color = Color.WHITE;

        int white = color.getRGB();
        color = Color.BLACK;

        int black = color.getRGB();
        color = Color.MAGENTA;

        image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        int[] data = image.getRGB(0, 0,width, height, null, 0, width);

        /*Flood fill*/
        for(int x = 0; x < width; x++)
        {
            for(int z = 0; z < height; z++)
            {
                data[x * height + z] = white;
            }
        }
        /*Horizontal lines for grid !Symmetric*/
        for(int z = height / 2; z < height; z += gridHeight)
        {
            for(int u = 0; u < width; u++)
            {
                data[z * width + u] = green;
            }
        }
        for(int z = height / 2; z > 0; z -= gridHeight)
        {
            for(int u = 0; u < width; u++)
            {
                data[z * width + u] = green;
            }
        }
        /*Vertical lines for grid !Symmetric*/
        for(int u = width / 2; u < width; u += gridWidth)
        {
            for(int z = 0; z < height; z++)
            {
                data[u + (width * z)] = green;
            }
        }

        for(int u = width / 2; u > 0; u -= gridWidth)
        {
            for(int z = 0; z < height; z++)
            {
                data[u + (width * z)] = green;
            }
        }
        /*Abscissa*/
       int half = height/2;
       for(int u = 0; u < width; u++)
        {
            data[u + half*width] = black;
        }

        /*Ordinate*/
        for(int x = 0; x < height; x++)
        {
            data[x * width + width / 2] = black;
        }

        int y = 0;
        double xScaled = width/(2.0*gridWidth);
        double sin = Math.sin((xScaled/(phase))) * amplitude;
        int yNext = (int)(height/2.0 + gridHeight * sin);

        int delta;
        /*
         Sinus drawing cycle.
         */
        for(int x = 0; x < width ; x++)
        {

            y = yNext;

            xScaled = (width/2.0  - x)/gridWidth;
            sin = Math.sin((xScaled/(phase))) * amplitude;

            yNext =(int) (height / 2.0 +  gridHeight * sin);
            delta = (yNext - y);


                for(int p = 0; p <= Math.abs(delta); p++)
                {
                    int point;
                    if(delta > 0)
                    {
                        point =  x + (y + p) * width;
                    }
                    else
                    {
                        point = x  + (y - p) * width;
                    }

                    if(point< NUMBER_OF_PIXELS && point > 0)
                    {
                        data[point] = GRAPH_COLOR;
                    }

                }
        }


        image.setRGB(0, 0, width, height, data, 0, width);

        TexturePaint tex = new TexturePaint(image, new Rectangle(0, 0, width, height));
        Graphics2D g2 = (Graphics2D) g;
        g2.setPaint(tex);
        g2.fillRect(0, 0, width, height);
    }

}
