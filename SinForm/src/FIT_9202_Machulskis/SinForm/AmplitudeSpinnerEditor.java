package FIT_9202_Machulskis.SinForm;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import java.awt.event.ActionEvent;

public
class AmplitudeSpinnerEditor extends SpinnerEditor
{
    JSlider slider;
    JSpinner sp;

    public
    AmplitudeSpinnerEditor(JSpinner spinner, SinusPane pane)
    {
        super(spinner, pane);
        spinner.addChangeListener(this);
        sp = spinner;

    }

    protected
    void addSliderListener(JSlider slider)
    {
        this.slider = slider;
    }

    protected
    void updateValue(int value)
    {
        sp.setValue(value);
    }

    @Override
    protected
    void updateValue(JSpinner spinner)
    {
        if(spinner.getEditor() instanceof AmplitudeSpinnerEditor)
        {
            ControlModel model = (ControlModel) spinner.getModel();
            Integer value = (Integer) spinner.getValue();
            if((Integer) model.getMaximum() < value)
            {
                spinner.setValue(model.getMaximum());
                slider.setValue((Integer) model.getMaximum());
            }
            else
            {
                pane.changeAmplitude(value);
                spinner.setValue(value);
                slider.setValue((Integer) value);
            }
        }
    }

    @Override
    public
    void stateChanged(ChangeEvent e)
    {
        super.stateChanged(e);
        JSpinner mySpinner = (JSpinner) (e.getSource());
        updateValue(mySpinner);

    }

}
