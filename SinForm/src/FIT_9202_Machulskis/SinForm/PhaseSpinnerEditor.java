package FIT_9202_Machulskis.SinForm;

import javax.swing.*;
import javax.swing.event.ChangeEvent;

public
class PhaseSpinnerEditor extends SpinnerEditor
{
    JSpinner sp;
    JSlider slider;

    public
    PhaseSpinnerEditor(JSpinner spinner, SinusPane pane)
    {
        super(spinner, pane);
        spinner.addChangeListener(this);
        sp = spinner;

    }

    @Override
    protected
    void updateValue(JSpinner spinner)
    {
        if(spinner.getEditor() instanceof PhaseSpinnerEditor)
        {
            ControlModel model = (ControlModel) spinner.getModel();
            Integer value = (Integer) spinner.getValue();

            if((Integer) model.getMaximum() < value)
            {
                spinner.setValue(model.getMaximum());
                slider.setValue((Integer) model.getMaximum());
            }
            else
            {
                pane.changePhase(value);
                spinner.setValue(value);
                slider.setValue(value);
            }
        }
    }

    protected
    void addSliderListener(JSlider slider)
    {
        this.slider = slider;
    }

    protected
    void updateValue(int value)
    {
        sp.setValue(value);
    }

    @Override
    public
    void stateChanged(ChangeEvent e)
    {
        super.stateChanged(e);
        JSpinner mySpinner = (JSpinner) (e.getSource());
        if(mySpinner.getEditor() instanceof PhaseSpinnerEditor)
        {
            updateValue(mySpinner);
        }
    }

}
