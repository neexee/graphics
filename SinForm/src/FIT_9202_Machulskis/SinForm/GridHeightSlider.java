package FIT_9202_Machulskis.SinForm;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public
class GridHeightSlider extends JSlider
{

    GridHeightSlider(final JSpinner ghSpinner, int minumum, int maximum, int value)
    {
        super(minumum, maximum);
        setValue(value);
        this.addChangeListener(new ChangeListener()
        {
            @Override
            public
            void stateChanged(ChangeEvent e)
            {
                {
                    JSlider slider = (JSlider) (e.getSource());
                    if(slider instanceof GridHeightSlider)
                    {
                        GridHeightSpinnerEditor spe = (GridHeightSpinnerEditor) ghSpinner.getEditor();
                        spe.updateValue(slider.getValue());
                    }
                }
            }
        });

    }


}
