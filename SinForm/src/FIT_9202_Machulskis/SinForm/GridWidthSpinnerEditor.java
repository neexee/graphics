package FIT_9202_Machulskis.SinForm;

import javax.swing.*;
import javax.swing.event.ChangeEvent;

public
class GridWidthSpinnerEditor extends SpinnerEditor
{
    JSlider slider;
    JSpinner sp;

    public
    GridWidthSpinnerEditor(JSpinner spinner, SinusPane pane)
    {
        super(spinner, pane);
        spinner.addChangeListener(this);
        sp = spinner;
    }

    @Override
    protected
    void updateValue(JSpinner spinner)
    {
        ControlModel model = (ControlModel) spinner.getModel();
        Integer value = (Integer) spinner.getValue();
        if((Integer) model.getMaximum() < value)
        {
            spinner.setValue(model.getMaximum());
            slider.setValue((Integer) model.getMaximum());
        }
        else
        {
            pane.changeWidth(value);
            spinner.setValue(value);
            slider.setValue((Integer) value);
        }

    }

    @Override
    public
    void stateChanged(ChangeEvent e)
    {
        super.stateChanged(e);
        JSpinner spinner = (JSpinner) (e.getSource());
        if(spinner.getEditor() instanceof GridWidthSpinnerEditor)
        {
            updateValue(spinner);
        }
    }

    protected
    void addSliderListener(JSlider slider)
    {
        this.slider = slider;
    }

    protected
    void updateValue(int value)
    {
        sp.setValue(value);
    }
}
