package FIT_9202_Machulskis.SinForm;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.io.*;
import java.util.Properties;
public
class ControlPanel extends JPanel
{
    Properties properties;
    JSpinner amplSpinner;
    JSpinner gridWidthSpinner;
    JSpinner phaseSpinner;
    JSpinner gridHeightSpinner;
    private final int  SPINNER_STEP = 1;
    private final String PANEL_TITLE = "Control";
    
    private final String AMPLITUDE_LABEL = "Amplitude";
    private final String PHASE_LABEL = "Phase";
    private final String GRID_WIDTH_LABEL = "Grid Width";
    private final String GRID_HEIGHT_LABEL = "Grid Height";
    
    private final String AMPLITUDE_KEYWORD = "Amplitude";
    private final String PHASE_KEYWORD = "Phase";
    private final String GRID_WIDTH_KEYWORD = "GridWidth";
    private final String GRID_HEIGHT_KEYWORD = "GridHeight";
    
    private final String CONFIG_COMMENT = "SinFrame properties";

    ControlPanel(SinusPane pane)
    {
        properties = new Properties();
        GridBagConstraints c = new GridBagConstraints();

        Border controlBorder = BorderFactory.createTitledBorder(PANEL_TITLE);
        setBorder(controlBorder);
        setLayout(new GridBagLayout());

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        JLabel phaseLabel = new JLabel(PHASE_LABEL);
        add(phaseLabel, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 1;

        phaseSpinner = new JSpinner(new ControlModel(SinusPane.MIN_PHASE,
                                                     SinusPane.MAX_PHASE, SinusPane.DEFAULT_PHASE, SPINNER_STEP ));

        PhaseSpinnerEditor sped = new PhaseSpinnerEditor(phaseSpinner, pane);

        phaseSpinner.setEditor(sped);
        add(phaseSpinner, c);

        JSlider phaseSlider = new PhaseSlider(phaseSpinner, SinusPane.MIN_PHASE, SinusPane.MAX_PHASE, SinusPane.DEFAULT_PHASE);
        sped.addSliderListener(phaseSlider);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 2;
        add(phaseSlider, c);


        JLabel amplLabel = new JLabel(AMPLITUDE_LABEL);
        c.gridx = 1;
        c.gridy = 0;
        add(amplLabel, c);
        ControlModel model = new ControlModel(SinusPane.MIN_AMPLITUDE,
                                                    SinusPane.MAX_AMPLITUDE, SinusPane.DEFAULT_AMPLITUDE, SPINNER_STEP);
        amplSpinner = new JSpinner(model);

        AmplitudeSpinnerEditor amplEditor = new AmplitudeSpinnerEditor(amplSpinner, pane);
        amplSpinner.setEditor(amplEditor);

        c.gridx = 1;
        c.gridy = 1;
        add(amplSpinner, c);
        JSlider amplSlider = new AmplitudeSlider(amplSpinner, SinusPane.MIN_AMPLITUDE, SinusPane.MAX_AMPLITUDE , SinusPane.DEFAULT_AMPLITUDE);
        amplEditor.addSliderListener(amplSlider);
        c.gridx = 1;
        c.gridy = 2;
        add(amplSlider, c);
        JLabel gwLabel = new JLabel(GRID_WIDTH_LABEL);
        c.gridx = 2;
        c.gridy = 0;
        add(gwLabel, c);
        gridWidthSpinner = new JSpinner(new ControlModel(SinusPane.MIN_GRID_WIDTH, SinusPane.MAX_GRID_WIDTH, SinusPane.DEFAULT_GRID_WIDTH, SPINNER_STEP));
        GridWidthSpinnerEditor gwspe = new GridWidthSpinnerEditor(gridWidthSpinner, pane);
        gridWidthSpinner.setEditor(gwspe);

        c.gridx = 2;
        c.gridy = 1;
        add(gridWidthSpinner, c);
        JSlider gwSlider = new GridWidthSlider(gridWidthSpinner, SinusPane.MIN_GRID_WIDTH, SinusPane.MAX_GRID_WIDTH, SinusPane.DEFAULT_GRID_WIDTH);
        gwspe.addSliderListener(gwSlider);
        c.gridx = 2;
        c.gridy = 2;
        add(gwSlider, c);


        JLabel ghLabel = new JLabel(GRID_HEIGHT_LABEL);
        c.gridx = 3;
        c.gridy = 0;
        add(ghLabel, c);
        gridHeightSpinner = new JSpinner(new ControlModel(SinusPane.MIN_GRID_HEIGHT, SinusPane.MAX_GRID_HEIGHT, SinusPane.DEFAULT_GRID_HEIGHT, SPINNER_STEP));
        GridHeightSpinnerEditor ghse = new GridHeightSpinnerEditor(gridHeightSpinner, pane);
        gridHeightSpinner.setEditor(ghse);
        c.gridx = 3;
        c.gridy = 1;
        add(gridHeightSpinner, c);
        JSlider ghSlider = new GridHeightSlider(gridHeightSpinner, SinusPane.MIN_GRID_HEIGHT, SinusPane.MAX_GRID_HEIGHT, SinusPane.DEFAULT_GRID_HEIGHT);
        ghse.addSliderListener(ghSlider);
        c.gridx = 3;
        c.gridy = 2;
        add(ghSlider, c);

        int height = phaseSpinner.getPreferredSize().height
                     + phaseLabel.getPreferredSize().height
                     + phaseSlider.getPreferredSize().height
                     + controlBorder.getBorderInsets(this).bottom
                     + controlBorder.getBorderInsets(this).top  ;
        int width  = phaseSlider.getPreferredSize().width
                     + gwSlider.getPreferredSize().width
                     + ghSlider.getPreferredSize().width
                     + amplSlider.getPreferredSize().width
                     + 2*controlBorder.getBorderInsets(this).right
                     + controlBorder.getBorderInsets(this).left;
        Dimension size = new Dimension(width, height);
        setMinimumSize(size);


    }

    public
    void savePropetries(File file) throws IOException
    {
        FileOutputStream stream = new FileOutputStream(file);
        properties = new Properties();
        properties.setProperty(GRID_HEIGHT_KEYWORD, gridHeightSpinner.getValue().toString());
        properties.setProperty(GRID_WIDTH_KEYWORD, gridWidthSpinner.getValue().toString());
        properties.setProperty(AMPLITUDE_KEYWORD, amplSpinner.getValue().toString());
        properties.setProperty(PHASE_KEYWORD, phaseSpinner.getValue().toString());

        properties.storeToXML(stream, CONFIG_COMMENT);
        stream.close();

    }

    public
    void applyConfig(File file) throws IOException
    {
        FileInputStream stream;
        try
        {
            stream = new FileInputStream(file);
        }
        catch(FileNotFoundException e)
        {
            e.printStackTrace();
        }


        stream = new FileInputStream(file);
        properties.loadFromXML(stream);
        String value;
        try
        {
            value = properties.getProperty(GRID_HEIGHT_KEYWORD,new Integer(SinusPane.DEFAULT_GRID_HEIGHT).toString());
            gridHeightSpinner.setValue(Integer.parseInt(value));
        }
        catch (NumberFormatException ex)
        {
           gridHeightSpinner.setValue(SinusPane.DEFAULT_GRID_HEIGHT);
        }
        try
        {
            value = properties.getProperty(GRID_WIDTH_KEYWORD, new Integer(SinusPane.DEFAULT_GRID_WIDTH).toString());
            gridWidthSpinner.setValue(Integer.parseInt(value));
        }
        catch (NumberFormatException ex)
        {
             gridWidthSpinner.setValue(SinusPane.DEFAULT_GRID_WIDTH);
        }
        try
        {
             value = properties.getProperty(AMPLITUDE_KEYWORD, new Integer(SinusPane.DEFAULT_AMPLITUDE).toString());
             amplSpinner.setValue(Integer.parseInt(value));
        }
        catch (NumberFormatException ex)
        {
            amplSpinner.setValue(SinusPane.DEFAULT_AMPLITUDE);
        }
        try
        {
             value = properties.getProperty(PHASE_KEYWORD, new Integer(SinusPane.DEFAULT_PHASE).toString());
             phaseSpinner.setValue(Integer.parseInt(value));
        }
        catch (NumberFormatException ex)
        {
            phaseSpinner.setValue(SinusPane.DEFAULT_PHASE);
        }
      stream.close();
    }
}
