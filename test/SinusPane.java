import java.awt.*;
import java.awt.image.*;
import javax.swing.*;

public class SinusPane extends JPanel {
    BufferedImage image;
    int width;
    int height;
    final int scaleX = 5;
    final int scaleY = 1;
    
    @Override
    public void paintComponent(Graphics g)
    {
        width = getSize().width;
        height = getSize().height;

        Color color =  Color.GREEN;
        int green = color.getRGB();
        color = Color.WHITE;
        int white = color.getRGB();

        int[] data = new int [width * height];

        for(int x = 0; x< width; x++)
        {
            for(int z =0; z < height; z++)
            {
                data[x*height+ z] =white;
            }
        } 
        for(int x = 0; x < height; x++)
        {
            data[x*width + (int)width/2] = green;
        }
        for(int u = 0; u < width; u++)
        {
            data[u + (int)(height*width/2)] = green; 
        }

        int y = 0;
        int yNext = 0;
        int delta;
        for (int x = 1; x < width-1; x++) 
        {

            y = yNext;
            yNext =  (int)((Math.sin((double)(x+1)*2.0*Math.PI/(width/scaleX) )+1.0)*(height-2)/(2.0*scaleY));
            delta = (int)((yNext - y)/2);

            for(int p = 0; p <= Math.abs(delta); p++)
            {

                if(delta > 0)
                {
                    data[x  + (y  + p)*width] =200;
                    data[(x + 1) + (y + delta  + p)*width] =200;
                }
                else
                {
                    data[x  + (y  - p)*width] =200;
                    data[(x + 1) + (y + delta  - p)*width] =200;
                }

            }
        }

        image = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);
        image.setRGB(0, 0, width, height, data, 0, width);

        TexturePaint tex =new TexturePaint(image, new Rectangle(0,0, width, height));
        Graphics2D g2  = (Graphics2D) g;
        g2.setPaint(tex);
        g2.fillRect(0, 0,width, height);
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Sinus");
        frame.add(new SinusPane());
        frame.setSize(100, 100);
        frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        frame.setVisible(true);
    }
}
