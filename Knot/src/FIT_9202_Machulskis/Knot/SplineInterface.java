package FIT_9202_Machulskis.Knot;

import java.util.Vector;

/**
 * User: violetta
 * Date: 5/18/12
 * Time: 9:25 AM
 */
public interface SplineInterface
{
    public Vector3 valueAt(double param);
}
