package FIT_9202_Machulskis.Knot;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Properties;
public
class ControlPanel extends JPanel
{
    Properties properties;

    KnotPanel panel;
    JSpinner angleOXSpinner;
    JSpinner angleOYSpinner;
    JSpinner RSpinner;
    Slider angleOXSlider;
    Slider angleOYSlider;
    private final String R_LABEL = "R";

    private final String ANGLE_OX_LABEL = "Angle OX";
    private final String ANGLE_OY_LABEL = "Angle OY";

    ControlPanel(final KnotPanel pane)
    {
        this.panel = pane;
        properties = new Properties();
        GridBagConstraints c = new GridBagConstraints();
        final int SPINNER_STEP = 1;
        final String PANEL_TITLE = "Control";
        Border controlBorder = BorderFactory.createTitledBorder(PANEL_TITLE);
        setBorder(controlBorder);
        setLayout(new GridBagLayout());
/*X spinner/slider */
        c.gridx = 0;
        c.gridy = 0;
        JLabel x1Label = new JLabel(ANGLE_OX_LABEL);
        add(x1Label, c);

        c.gridx = 0;
        c.gridy = 1;


        angleOXSpinner = new JSpinner(new ControlModel(KnotPanel.MIN_ANGLE,
                KnotPanel.MAX_ANGLE, KnotPanel.DEFAULT_ANGLE_OX, SPINNER_STEP));

        SpinnerEditor sp = new SpinnerEditor(angleOXSpinner)
        {
            @Override
            public void makeSomethingWithPane(double value)
            {
                pane.setAngleOX(value);
            }
        };

        angleOXSpinner.setEditor(sp);
        add(angleOXSpinner, c);

        angleOXSlider = new Slider(angleOXSpinner, KnotPanel.MIN_ANGLE, KnotPanel.MAX_ANGLE, KnotPanel.DEFAULT_ANGLE_OX);
        sp.addSliderListener(angleOXSlider);
        c.gridx = 0;
        c.gridy = 2;
        add(angleOXSlider, c);

/*Y spinner/slider */
        c.gridx = 1;
        c.gridy = 0;
        JLabel y1Label = new JLabel(ANGLE_OY_LABEL);
        add(y1Label, c);

        c.gridx = 1;
        c.gridy = 1;

        angleOYSpinner = new JSpinner(new ControlModel(KnotPanel.MIN_ANGLE,
                KnotPanel.MAX_ANGLE, KnotPanel.DEFAULT_ANGLE_OY, SPINNER_STEP));

        SpinnerEditor spedy1 = new SpinnerEditor(angleOYSpinner)
        {
            @Override
            public void makeSomethingWithPane(double value)
            {
                pane.setAngleOY(value);
            }
        };

        angleOYSpinner.setEditor(spedy1);
        add(angleOYSpinner, c);

        angleOYSlider = new Slider(angleOYSpinner, KnotPanel.MIN_ANGLE, KnotPanel.MAX_ANGLE, KnotPanel.DEFAULT_ANGLE_OY);
        spedy1.addSliderListener(angleOYSlider);
        c.gridx = 1;
        c.gridy = 2;
        add(angleOYSlider, c);

/*R spinner/slider */
        c.gridx = 2;
        c.gridy = 0;
        JLabel phaseLabel = new JLabel(R_LABEL);
        add(phaseLabel, c);

        c.gridx = 2;
        c.gridy = 1;

        RSpinner = new JSpinner(new ControlModel(KnotPanel.MIN_R,

                                                     KnotPanel.MAX_R, KnotPanel.DEFAULT_R, SPINNER_STEP));

        SpinnerEditor sped = new SpinnerEditor(RSpinner)
        {
            @Override
            public void makeSomethingWithPane(double value)
            {
                pane.setR(value);
            }
        };

        RSpinner.setEditor(sped);
        add(RSpinner, c);

        JSlider RSlider = new Slider(RSpinner, KnotPanel.MIN_R, KnotPanel.MAX_R, KnotPanel.DEFAULT_R);
        sped.addSliderListener(RSlider);
        c.gridx = 2;
        c.gridy = 2;
        add(RSlider, c);
        Dimension size = new Dimension(getPreferredSize());
        setMinimumSize(size);
        final JCheckBox secondSplineCheckBox = new JCheckBox("Enable second");
        c.gridx =3;
        c.gridy = 0;
        c.weighty = 3;
        add(secondSplineCheckBox, c);
        secondSplineCheckBox.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                if(secondSplineCheckBox.isEnabled())
                {
                    panel.setDrawSecondSpline(true);
                }
                else
                {
                    panel.setDrawSecondSpline(false);
                }
            }
        });

    }
   KnotPanel getKnotPanel()
   {
       return panel;
   }
   double getAngleOX()
   {
       return panel.getAngleOX();
   }
   double getAngleOY()
   {
       return panel.getAngleOY();
   }
   double getR()
   {
       return panel.getR();
   }
    void setAngleOX(double angle)
    {
        angleOXSpinner.setValue(angle);
    }
    void setAngleOY(double angle)
    {
        angleOYSpinner.setValue(angle);
    }
    void setR(double value)
    {
        RSpinner.setValue(value);
    }
}