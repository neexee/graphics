package FIT_9202_Machulskis.Knot;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.Vector;

public class KnotPanel extends JPanel
{

    public final static int DEFAULT_R = 10;
    public final static int MIN_R = 0;
    public final static int MAX_R = 100000;

    public final static int DEFAULT_ANGLE_OX = 0;
    public final static int DEFAULT_ANGLE_OY = 0;
    public final static int MIN_ANGLE = 0;
    public final static int MAX_ANGLE = 360;

    public final static int MINIMAL_SIZE = 0;

    BufferedImage image;

    int width = 0;
    int height = 0;
    int numSplines = 30;
    double near = 5.0;

    boolean drawSecond = false;
    private double angleOX = DEFAULT_ANGLE_OX;
    private double angleOY = DEFAULT_ANGLE_OY;
    private double R = DEFAULT_R;
    Knot knot;

    KnotPanel(int r)
    {
        R = r;
        angleOX = 35.0f;
        near = 5;
        R = 2.0f;

    }

    void setAngleOX(double x)
    {
        angleOX = x;
        repaint();
    }

    void setAngleOY(double y)
    {
        angleOY = y;
        repaint();
    }

    double getAngleOX()
    {
        return angleOX;
    }

    double getAngleOY()
    {
        return angleOY;
    }

    public void setR(double R)
    {
        this.R = R;
        repaint();
    }

    public double getR()
    {
        return R;
    }
    void setDrawSecondSpline(boolean f)
    {
        drawSecond = f;
    }
    @Override
    public void paintComponent(Graphics g)
    {
        this.width = getSize().width;
        this.height = getSize().height;

        Color color = Color.GREEN;

        int green = color.getRGB();
        color = Color.WHITE;

        int white = color.getRGB();
        color = Color.BLACK;

        int black = color.getRGB();

        image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        int[] data = image.getRGB(0, 0, width, height, null, 0, width);

        /*Flood fill*/
        for(int x = 0; x < width; x++)
        {
            for(int z = 0; z < height; z++)
            {
                data[x * height + z] = white;
            }
        }

        for(int u = 0; u < width; u++)
        {
            data[(height / 2) * width + u] = green;
        }

        for(int z = 0; z < height; z++)
        {
            data[width / 2 + (width * z)] = green;
        }

        /*Abscissa*/
        int half = height / 2;
        for(int u = 0; u < width; u++)
        {
            data[u + half * width] = black;
        }

        /*Ordinate*/
        for(int x = 0; x < height; x++)
        {
            data[x * width + width / 2] = black;
        }

        image.setRGB(0, 0, width, height, data, 0, width);

        double minX = Double.MAX_VALUE;
        double maxX = Double.MIN_VALUE;
        double minY = Double.MAX_VALUE;
        double maxY = Double.MIN_VALUE;
        double minZ = Double.MAX_VALUE;
        double maxZ = Double.MIN_VALUE;


        knot = new Knot();
        double from = knot.from();
        double to = knot.to();
        int degree = 3;
        double step = (from - to) / (numSplines - 1);
        for(int i = 0; i < numSplines; i++)
        {
            double param = step * i + from;
            Vector<Vector3> control = new Vector<Vector3>();

            for(int j = 0; j < degree; j++)
            {
                double subparam = param + j * step / (degree - 1);
                Vector3 point3d = knot.valueAt(subparam);

                control.add(point3d);
            }
            Spline spline = new Spline(control);

            double begin = 0.0;
            double end = 1.0;

            Vector3 begin3d = spline.valueAt(begin);
            Vector3 end3d = spline.valueAt(end);

            Point begin2d = project(begin3d);
            Point end2d = project(end3d);

            if(isVisible(begin3d))
            {
                setPoint(begin2d.x, begin2d.y, Color.RED.getRGB());
            }

            if(isVisible(end3d))
            {
                setPoint(end2d.x, end2d.y, Color.RED.getRGB());
            }

            Pair<Double, Point> beginCached = new Pair<Double, Point>(begin, begin2d);
            Pair<Double, Point> endCached = new Pair<Double, Point>(end, end2d);

            LinkedList<Pair<Pair<Double, Point>, Pair<Double, Point>>> queue = new LinkedList<Pair<Pair<Double, Point>, Pair<Double, Point>>>();
            queue.addLast(new Pair<Pair<Double, Point>, Pair<Double, Point>>(beginCached, endCached));

            minX = Math.min(minX, Math.min(begin3d.x(), end3d.x()));
            maxX = Math.max(maxX, Math.max(begin3d.x(), end3d.x()));
            minY = Math.min(minY, Math.min(begin3d.y(), end3d.y()));
            maxY = Math.max(maxY, Math.max(begin3d.y(), end3d.y()));
            minZ = Math.min(minZ, Math.min(begin3d.z(), end3d.z()));
            maxZ = Math.max(maxZ, Math.max(begin3d.z(), end3d.z()));

            while(!queue.isEmpty())
            {
                Pair<Pair<Double, Point>, Pair<Double, Point>> interval = queue.getFirst();

                double start = interval.first.first;
                double finish = interval.second.first;
                double middle = (start + finish) / 2;
                queue.removeFirst();

                Point start2d = interval.first.second;
                Point finish2d = interval.second.second;

                if(Math.abs(start2d.x - finish2d.x) > 1 || Math.abs(start2d.y - finish2d.y) > 1)
                {
                    Vector3 middle3d = spline.valueAt(middle);
                    Point middle2d = project(middle3d);

                    setPoint(middle2d.x, middle2d.y, Color.RED.getRGB());


                    Pair<Double, Point> middleCached = new Pair<Double, Point>(middle, middle2d);

                    queue.addLast(new Pair<Pair<Double, Point>, Pair<Double, Point>>(interval.first, middleCached));
                    queue.addLast(new Pair<Pair<Double, Point>, Pair<Double, Point>>(middleCached, interval.second));

                    minX = Math.min(minX, middle3d.x());
                    maxX = Math.max(maxX, middle3d.x());
                    minY = Math.min(minY, middle3d.y());
                    maxY = Math.max(maxY, middle3d.y());
                    minZ = Math.min(minZ, middle3d.z());
                    maxZ = Math.max(maxZ, middle3d.z());
                }
            }
        }

        TexturePaint tex = new TexturePaint(image, new Rectangle(0, 0, width, height));
        Graphics2D g2 = (Graphics2D) g;
        g2.setPaint(tex);
        g2.fillRect(0, 0, width, height);
    }

    boolean isValidPosition(int x, int y)
    {
        return (x >= 0 && x < image.getWidth() && y >= 0 && y < image.getHeight());
    }

    private void setPoint(int x, int y, int color)
    {
        if(isValidPosition(x, y))
        {
            image.setRGB(x, y, color);
        }

    }
    Point project(Vector3 point3d)
    {
        final double t = angleOX * Math.PI / 180.0;
        final double p = angleOY * Math.PI / 180.0;
        Vector3 viewpoint = new Vector3(0, 0, 0);
        final double cosT = Math.cos(t);
        final double sinT = Math.sin(t);
        final double cosP = Math.cos(p);
        final double sinP = Math.sin(p);

        final double cosTcosP = cosT * cosP;
        final double cosTsinP = cosT * sinP;
        final double sinTcosP = sinT * cosP;
        final double sinTsinP = sinT * sinP;

        final double imageW = width;
        final double imageH = height;
        final double scale = Math.min(imageW, imageH) / 4;

        final double x0 = point3d.x() - viewpoint.x();
        final double y0 = point3d.y() - viewpoint.y();
        final double z0 = point3d.z() - viewpoint.z();

        final double x1 = cosT * x0 + sinT * z0;
        final double y1 = -sinTsinP * x0 + cosP * y0 + cosTsinP * z0;
        final double z1 = cosTcosP * z0 - sinTcosP * x0 - sinP * y0;

        final double tempX = x1 * near / (z1 + near + R);
        final double tempY = y1 * near / (z1 + near + R);

        final Point point = new Point((int) (imageW / 2 + scale * tempX + 0.5), (int) (imageH / 2 - scale * tempY + 0.5));

        return point;
    }

    boolean clip(Vector3 first, Vector3 second)
    {
        boolean firstVisible = isVisible(first);
        boolean secondVisible = isVisible(second);

        if(!firstVisible && !secondVisible)
        {
            return false;
        }

        if(firstVisible && secondVisible)
        {
            return true;
        }

        if(secondVisible)
        {
            Vector3 tmp = first;
            first = second;
            second = tmp;
        }

        double distanceFirst = distance(first);
        double distanceSecond = distance(second);
        double ditance = distanceFirst + distanceSecond;

        second = first.mul(distanceSecond / ditance).add(second.mul(distanceFirst / ditance));

        return true;
    }

    boolean isVisible(Vector3 point3d)
    {

        double t = (-90 + angleOX) * Math.PI / 180.0;
        double p = (90 - angleOY) * Math.PI / 180.0;

        final double centerX = R * Math.sin(p) * Math.cos(t);
        final double centerZ = R * Math.sin(p) * Math.sin(t);
        final double centerY = R * Math.cos(p);

        final double eyeX = (R + near) * Math.sin(p) * Math.cos(t);
        final double eyeZ = (R + near) * Math.sin(p) * Math.sin(t);
        final double eyeY = (R + near) * Math.cos(p);

        final Vector3 center = new Vector3(centerX, centerY, centerZ);
        final Vector3 eye = new Vector3(eyeX, eyeY, eyeZ);

        return point3d.distanceToPlane(center, center) * eye.distanceToPlane(center, center) < 0;
    }

    double distance(Vector3 point3d)
    {

        double t = (-90 + angleOX) * Math.PI / 180.0;
        double p = (90 - angleOY) * Math.PI / 180.0;

        double centerX = R * Math.sin(p) * Math.cos(t);
        double centerZ = R * Math.sin(p) * Math.sin(t);
        double centerY = R * Math.sin(p);

        Vector3 center = new Vector3(centerX, centerY, centerZ);

        return Math.abs(point3d.distanceToPlane(center, center));
    }
}
