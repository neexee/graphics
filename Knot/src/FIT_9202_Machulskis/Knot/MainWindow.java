package FIT_9202_Machulskis.Knot;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;

public
class MainWindow extends JFrame
{
    final String PANEL_TITLE = "Knot";
    final String MENU_NAME = "File"; 
    final String OPEN_FILE = "Open config ..";
    final String SAVE_FILE = "Save config ..";
    final String EXIT = "Exit";
    JMenuBar menuBar;
    JPanel mainPanel;
    KnotPanel knotPane;
    ControlPanel control;
    JFileChooser fc;

    public MainWindow(String name)
    {
        super(name);
    }

    public
    void drawGUI()
    {

        mainPanel = new JPanel();    
        mainPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        
        /*Create panel with knot*/
        JPanel knotPaneWrapper = new JPanel();
        knotPaneWrapper.setLayout(new GridBagLayout());
        Border sinBorder = BorderFactory.createTitledBorder(PANEL_TITLE);
        knotPaneWrapper.setBorder(sinBorder);
        knotPane = new KnotPanel(KnotPanel.DEFAULT_R);

        knotPaneWrapper.setPreferredSize(new Dimension(mainPanel.getWidth(), mainPanel.getHeight()));
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1;
        c.weighty = 1;
        knotPaneWrapper.add(knotPane,c );

        mainPanel.add(knotPaneWrapper, c);

        /*Create control panel*/
        control = new ControlPanel(knotPane);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 1;
        c.weighty =0;
        c.anchor= GridBagConstraints.SOUTHEAST;
        mainPanel.add(control, c);


        menuBar = new JMenuBar();
        fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        JMenu menu = new JMenu(MENU_NAME);

        JMenuItem exit = new JMenuItem(EXIT);
        exit.addActionListener(new ActionListener()
        {
            @Override
            public
            void actionPerformed(ActionEvent e)
            {
                MainWindow.this.dispose();
            }
        });
        /*
        JMenuItem openFile = new JMenuItem(OPEN_FILE);

        openFile.addActionListener(new ActionListener()
        {
            @Override
            public
            void actionPerformed(ActionEvent e)
            {
                int returnVal = fc.showOpenDialog(MainWindow.this);

                if(returnVal==JFileChooser.APPROVE_OPTION)
                {
                    File file = fc.getSelectedFile();
                    try
                    {
                        control.applyConfig(file);
                    }
                    catch(IOException e1)
                    {
                        JOptionPane.showMessageDialog(MainWindow.this, e1.getLocalizedMessage());
                    }

                }
            }
        });
        JMenuItem saveFile = new JMenuItem(SAVE_FILE);

        saveFile.addActionListener(new ActionListener()
        {
            @Override
            public
            void actionPerformed(ActionEvent e)
            {
                int returnVal = fc.showSaveDialog(MainWindow.this);

                if(returnVal==JFileChooser.APPROVE_OPTION)
                {
                    File file = fc.getSelectedFile();
                    try
                    {
                       control.savePropetries(file);
                    }
                    catch(IOException e1)
                    {
                        JOptionPane.showMessageDialog(MainWindow.this, e1.getLocalizedMessage());
                    }

                }
            }
        });
*/
        menu.add(exit);
   //     menu.add(openFile);
//        menu.add(saveFile);
        menuBar.add(menu);
        setJMenuBar(menuBar);
        getContentPane().add(mainPanel);
        FocusPickListener listener = new FocusPickListener(control);
        addMouseMotionListener(listener);
        addMouseListener(listener);
        pack();

        setMinimumSize(new Dimension(control.getMinimumSize().width + getInsets().left + getInsets().right,
                          control.getMinimumSize().height
                            + getInsets().top + getInsets().bottom
                            + menuBar.getHeight()));
        setPreferredSize(new Dimension(KnotPanel.MINIMAL_SIZE
                                         + control.getPreferredSize().width,
                                         control.getPreferredSize().height));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

}
