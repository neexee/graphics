package FIT_9202_Machulskis.Knot;

/**
 * User: violetta
 * Date: 5/10/12
 * Time: 11:54 PM
 */
public class Vector3
{
    double xp = 0;
    double yp = 0;
    double zp = 0;

    public Vector3(double xp, double yp, double zp)
    {
        this.xp = xp;
        this.yp = yp;
        this.zp = zp;
    }

    public double distanceToPlane(Vector3 plane, Vector3 normal)
    {
        return dotProduct(this.delta(plane), normal);
    }

    Vector3 delta(Vector3 v)
{
    return new  Vector3(this.xp - v.xp, this.yp - v.yp, this.zp - v.zp);
}
    double dotProduct(Vector3 v1, Vector3 v2)
    {
        return v1.xp * v2.xp + v1.yp * v2.yp + v1.zp * v2.zp;
    }
    Vector3 mul(double factor)
    {
         return new Vector3(xp * factor, yp * factor, zp * factor);
    }
    Vector3 add(Vector3 v)
    {
     return new Vector3(xp + v.xp, yp + v.yp, zp + v.zp);
    }
    public double x()
    {
        return xp;
    }
    public double y()
    {
        return yp;
    }
    public double z()
    {
        return zp;
    }
}
