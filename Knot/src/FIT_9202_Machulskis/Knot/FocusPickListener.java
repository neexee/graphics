package FIT_9202_Machulskis.Knot;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class FocusPickListener implements MouseMotionListener, MouseListener
{
    Point pickPoint = new Point();
    ControlPanel control;
    KnotPanel panel;
    
    public FocusPickListener(ControlPanel panel)
    {
       this.panel = panel.getKnotPanel();
       control = panel;
    }
    @Override
    public void mouseClicked(MouseEvent mouseEvent)
    {

    }

    @Override
    public void mousePressed(MouseEvent mouseEvent)
    {
        Point pressPoint = mouseEvent.getLocationOnScreen();
        Point panelLocation = panel.getLocationOnScreen();
        pressPoint.x -= panelLocation.x;
        pressPoint.y -=panelLocation.y;
        pickPoint = pressPoint;


    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent)
    {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent)
    {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent)
    {

    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent)
    {
        Point dragPoint = mouseEvent.getLocationOnScreen();
        Point panelLocation = panel.getLocationOnScreen();
        dragPoint.x -= panelLocation.x;
        dragPoint.y -=panelLocation.y;
        double deltax = ((double)pickPoint.x - dragPoint.x)/panel.width*180.0;
        double deltay =  ((double) pickPoint.y - dragPoint.y)/panel.height*180.0;
        double lastx = control.getAngleOX() + deltax;
        double lasty = control.getAngleOY() + deltay;
        if(lastx >360.0)
        {
            lastx-=360.0;
        }
        if(lastx < 0)
        {
            lastx +=360.0;
        }
        if(lasty >360.0)
        {
            lasty-=360.0;
        }
        if(lasty < 0)
        {
            lasty +=360.0;
        }
        control.setAngleOX(lastx);
        control.setAngleOY(lasty);
        pickPoint= dragPoint;
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent)
    {

    }
}
