package FIT_9202_Machulskis.Knot;

import FIT_9202_Machulskis.Knot.Vector3;

/**
 * User: violetta
 * Date: 5/17/12
 * Time: 1:13 AM
 */
public class Knot
{
    double from()
    {
        return 0;
    }

    double to()
    {
        return 2 * Math.PI;
    }
    /*This is trefoil knot*/
    Vector3 valueAt(double param)
    {
        final double x = Math.sin(param) + 2 * Math.sin(2 * param);
        final double y = Math.cos(param) - 2 * Math.cos(2 * param);
        final double z = Math.sin(3 * param);

        return new Vector3(x, y, z);
    }
}
