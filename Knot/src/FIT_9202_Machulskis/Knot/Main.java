package FIT_9202_Machulskis.Knot;

import javax.swing.*;
public class Main
{


    public static void main(String[] args)
    {
                SwingUtilities.invokeLater(new Runnable()
                {
                    public
                    void run()
                    {
                         UIManager.put("swing.boldMetal", Boolean.FALSE);
                         MainWindow mainWindow = new MainWindow("FIT_9202_Machulskis.Knot.Knot");
                         mainWindow.drawGUI();
                    }
                });
    }
}