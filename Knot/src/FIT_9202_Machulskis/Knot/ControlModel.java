package FIT_9202_Machulskis.Knot;

import javax.swing.*;

public class ControlModel extends SpinnerNumberModel
{
    double value;
    double step;
    double maximum;
    double minimum;


    public ControlModel(double minumum, double maximum,double value, double step)
    {
        super(value, minumum, maximum, step);
        this.minimum = minumum;
        this.maximum = maximum;
        this.value = value;
        this.step = step;

    }
    double getMax()
    {
        return maximum;
    }
    double getMin()
    {
        return minimum;
    }



}
