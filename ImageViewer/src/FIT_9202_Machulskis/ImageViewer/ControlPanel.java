package FIT_9202_Machulskis.ImageViewer;

import FIT_9202_Machulskis.ImageViewer.Control.ControlModel;
import FIT_9202_Machulskis.ImageViewer.Control.Slider;
import FIT_9202_Machulskis.ImageViewer.Control.SpinnerEditor;
import FIT_9202_Machulskis.ImageViewer.ImageDraw.ImageModel;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class ControlPanel extends JPanel
{
    JSpinner positionYSpinner;
    JSpinner positionXSpinner;
    JSpinner angleSpinner;
    JSpinner scaleXSpinner;
    JSpinner scaleYSpinner;
    JSlider positionXSlider;
    JSlider positionYSlider;
    JSlider angleSlider;
    JSlider scaleYSlider;
    JSlider scaleXSlider;
    private final double SPINNER_STEP = 0.1;
    private final String PANEL_TITLE = "Control";
    
    private final String POSITION_X_LABEL = "Position X";
    private final String POSITION_Y_LABEL = "Position Y";
    private final String ANGLE_LABEL = "Angle";
    private final String SCALE_X_LABEL = "Scale X";
    private final String SCALE_Y_LABEL = "Scale Y";


    ControlPanel(final ImageModel model)
    {
        GridBagConstraints c = new GridBagConstraints();

        Border controlBorder = BorderFactory.createTitledBorder(PANEL_TITLE);
        setBorder(controlBorder);
        setLayout(new GridBagLayout());

        // Prepare position X slider, spinner, slider
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        JLabel positionXLabel = new JLabel(POSITION_X_LABEL);
        add(positionXLabel, c);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 1;

        positionXSpinner = new JSpinner(new ControlModel(ImageModel.MIN_POSITION,
                                                         ImageModel.MAX_POSITION,
                                                         ImageModel.DEFAULT_POSITION,
                                                         SPINNER_STEP ));

        SpinnerEditor sped = new SpinnerEditor(positionXSpinner)
        {
            @Override
            public void makeSomethingWithPane(double value)
            {
                model.setPositionX(value);
            }
        };

        positionXSpinner.setEditor(sped);
        add(positionXSpinner, c);

        positionXSlider = new Slider(positionXSpinner,
                                             ImageModel.MIN_POSITION,
                                             ImageModel.MAX_POSITION,
                                             ImageModel.DEFAULT_POSITION);
        sped.addSliderListener(positionXSlider);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 2;
        add(positionXSlider, c);

        // Prepare position Y slider, spinner, slider

        JLabel positionYlLabel = new JLabel(POSITION_Y_LABEL);
        c.gridx = 1;
        c.gridy = 0;
        add(positionYlLabel, c);

        positionYSpinner = new JSpinner(new ControlModel(ImageModel.MIN_POSITION,
                                                         ImageModel.MAX_POSITION,
                                                         ImageModel.DEFAULT_POSITION,
                                                         SPINNER_STEP ));

        SpinnerEditor positionYEditor = new SpinnerEditor(positionYSpinner)
        {
            @Override
            public void makeSomethingWithPane(double value)
            {
                 model.setPositionY(value);
            }
        };
        positionYSpinner.setEditor(positionYEditor);

        c.gridx = 1;
        c.gridy = 1;
        add(positionYSpinner, c);

        positionYSlider = new Slider(positionYSpinner,
                                             ImageModel.MIN_POSITION,
                                             ImageModel.MAX_POSITION,
                                             ImageModel.DEFAULT_POSITION);

        positionYEditor.addSliderListener(positionYSlider);
        c.gridx = 1;
        c.gridy = 2;
        add(positionYSlider, c);

        // Prepare angle slider, spinner, slider

        JLabel angleLabel = new JLabel(ANGLE_LABEL);
        c.gridx = 2;
        c.gridy = 0;
        add(angleLabel, c);
        angleSpinner = new JSpinner(new ControlModel(ImageModel.MIN_ANGLE,
                                                     ImageModel.MAX_ANGLE,
                                                     ImageModel.DEFAULT_ANGLE,
                                                     SPINNER_STEP));

        SpinnerEditor angleEditor = new SpinnerEditor(angleSpinner)
        {
            @Override
            public void makeSomethingWithPane(double value)
            {
                model.setAngle(value);
            }
        };

        angleSpinner.setEditor(angleEditor);

        c.gridx = 2;
        c.gridy = 1;
        add(angleSpinner, c);

        angleSlider = new Slider(angleSpinner,
                                         ImageModel.MIN_ANGLE,
                                         ImageModel.MAX_ANGLE,
                                         ImageModel.DEFAULT_ANGLE);

        angleEditor.addSliderListener(angleSlider);
        c.gridx = 2;
        c.gridy = 2;
        add(angleSlider, c);

        // Prepare scale X slider, spinner, slider
        JLabel scaleXLabel = new JLabel(SCALE_X_LABEL);
        c.gridx = 3;
        c.gridy = 0;
        add(scaleXLabel, c);
        scaleXSpinner = new JSpinner(new ControlModel(ImageModel.MIN_SCALE,
                                                      ImageModel.MAX_SCALE,
                                                      ImageModel.DEFAULT_SCALE,
                                                      SPINNER_STEP));

        SpinnerEditor scaleXEditor = new SpinnerEditor(scaleXSpinner)
        {
            @Override
            public void makeSomethingWithPane(double value)
            {
                model.setScaleX(value);
            }
        };
        scaleXSpinner.setEditor(scaleXEditor);
        c.gridx = 3;
        c.gridy = 1;
        add(scaleXSpinner, c);
        scaleXSlider = new Slider(scaleXSpinner,
                                          ImageModel.MIN_SCALE,
                                          ImageModel.MAX_SCALE,
                                          ImageModel.DEFAULT_SCALE);

        scaleXEditor.addSliderListener(scaleXSlider);
        c.gridx = 3;
        c.gridy = 2;
        add(scaleXSlider, c);


        // Prepare scale Y slider, spinner, slider
        JLabel scaleYLabel = new JLabel(SCALE_Y_LABEL);
        c.gridx = 4;
        c.gridy = 0;
        add(scaleYLabel, c);
        scaleYSpinner = new JSpinner(new ControlModel(ImageModel.MIN_SCALE,
                                                      ImageModel.MAX_SCALE,
                                                      ImageModel.DEFAULT_SCALE,
                                                      SPINNER_STEP));
        SpinnerEditor scaleYEditor = new SpinnerEditor(scaleYSpinner)
        {
            @Override
            public void makeSomethingWithPane(double value)
            {
                model.setScaleY(value);
            }
        };
        scaleYSpinner.setEditor(scaleYEditor);
        c.gridx = 4;
        c.gridy = 1;
        add(scaleYSpinner, c);

        scaleYSlider = new Slider(scaleYSpinner,
                                          ImageModel.MIN_SCALE,
                                          ImageModel.MAX_SCALE,
                                          ImageModel.DEFAULT_SCALE);
        scaleYEditor.addSliderListener(scaleYSlider);
        c.gridx = 4;
        c.gridy = 2;
        add(scaleYSlider, c);
        int height =  scaleXSpinner.getPreferredSize().height
                     + scaleXLabel.getPreferredSize().height
                     + scaleXSlider.getPreferredSize().height
                     + controlBorder.getBorderInsets(this).bottom
                     + controlBorder.getBorderInsets(this).top  ;
        int width  = scaleXSlider.getPreferredSize().width
                     + scaleYSlider.getPreferredSize().width
                     + positionXSlider.getPreferredSize().width
                     + positionYSlider.getPreferredSize().width
                     + angleSlider.getPreferredSize().width
                     + 2*controlBorder.getBorderInsets(this).right
                     + controlBorder.getBorderInsets(this).left;
        Dimension size = new Dimension(width, height);
        setMinimumSize(size);


    }
    public JSlider getScaleYSlider()
    {
        return scaleYSlider;
    }
    public JSlider getScaleXSlider()
    {
        return scaleXSlider;
    }
    public JSlider getPositionXSlider()
    {
        return positionXSlider;
    }
    public JSlider getPositionYSlider()
    {
        return positionYSlider;
    }
    public JSlider getAngleSlider()
    {
        return angleSlider;
    }
}
