package FIT_9202_Machulskis.ImageViewer.ImageDraw;

import java.awt.*;
import java.util.Comparator;

/**
 * User: violetta
 * Date: 3/22/12
 * Time: 7:30 AM
 */
public class VerticleComparator implements Comparator<Pair<Point, Point>>
{
    @Override
    public int compare(Pair<Point, Point> first, Pair<Point, Point> second)
    {
      if (first.first.y != second.first.y) {
        return + first.first.y - second.first.y;
    }

    return +first.first.x - second.first.x;

    }
}
