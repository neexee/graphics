package FIT_9202_Machulskis.ImageViewer.ImageDraw;

import java.awt.image.BufferedImage;

public class NearestImagePanel extends AbstractImagePanel
{
    public NearestImagePanel(ImageModel model1)
    {
        super(model1);
    }

    @Override
    int getPixelColor(double x_, double y_, BufferedImage t)
    {
        int sx = t.getWidth();
        int sy = t.getHeight();
        final int ix = (int)Math.floor(x_ - 0.5);
        final int iy = (int) Math.floor(y_ - 0.5);
        if( (ix < 0) || (ix >= sx) || (iy < 0) || (iy >= sy))
        {
            return 0xFFFFFF;
        }
       return texture.getRGB(ix, iy);
    }

    @Override
    void textureChanged()
    {
    }
}
