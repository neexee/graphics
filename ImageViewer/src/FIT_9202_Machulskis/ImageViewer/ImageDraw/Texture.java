package FIT_9202_Machulskis.ImageViewer.ImageDraw;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * User: violetta
 * Date: 3/22/12
 * Time: 6:54 AM
 */
public class Texture
{   ArrayList<Point> points;

    BufferedImage texture;


Texture(BufferedImage image, Point first, Point second, Point thrid)
{
    points = new ArrayList<Point>();
    texture = image;
    points.add(first);
    points.add(second);
    points.add(thrid);
}

BufferedImage image() 
{
    return texture;
}

Point first()  {
    return points.get(0);
}

Point second(){
    return points.get(1);
}

Point thrid()
{
    return points.get(2);
}

}
