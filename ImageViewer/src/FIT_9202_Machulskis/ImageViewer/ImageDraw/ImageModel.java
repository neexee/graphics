package FIT_9202_Machulskis.ImageViewer.ImageDraw;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class ImageModel
{
   private double positionX;
   private double positionY;
   private double angle;
   private double scaleX;
   private double scaleY;
   private boolean controlPointSet = false;
   private BufferedImage texture;
   private Point draggedPoint = new Point();
   private Point controlPoint = new Point();
   private boolean draggedPointSet = false;
   private boolean textureChanged = false;

   ArrayList<AbstractImagePanel> panels;
   public static final double MAX_POSITION = 1000;
   public static final double MIN_POSITION = -1000;
   public static final double DEFAULT_POSITION = 0;
   
   public static final double MAX_ANGLE = 360;
   public static final double MIN_ANGLE = 0;
   public static final double DEFAULT_ANGLE = 0;
    
   public static final double MAX_SCALE = 100;
   public static final double MIN_SCALE = -100;
   public static final double DEFAULT_SCALE = 1;
   public ImageModel()
   {
       positionX = DEFAULT_POSITION;
       positionY = DEFAULT_POSITION;
       angle = DEFAULT_ANGLE;
       scaleX = DEFAULT_SCALE;
       scaleY = DEFAULT_SCALE;
       controlPoint = new Point();
       panels = new ArrayList<AbstractImagePanel>();
       
   }
   public void setDraggedPoint(Point p)
   {
     draggedPoint =p;
     draggedPointSet =true;
   }
   public Point getDraggedPoint()
   {
       return draggedPoint;
   }
   public void  setPositionX(double x)
   {
       positionX = x;
       notifyAllObservers();
   }
   public double getPositionY()
   {
       return positionY;

   }
   public void  setPositionY(double y)
   {
       positionY = y;
       notifyAllObservers();
   }
   public double getPositionX()
   {
       return positionX;
   }
   public void  setAngle(double angle)
   {
       this.angle = angle;
       notifyAllObservers();
   }
   public double getAngle()
   {
       return angle;
   }
   public void setScaleX(double scaleX)
   {
       this.scaleX = scaleX;
       notifyAllObservers();
   }
   public double getScaleX()
   {
       return scaleX;
   }
   public void  setScaleY(double scaleY)
   {
       this.scaleY =scaleY;
       notifyAllObservers();
   }
   public double getScaleY()
    {
       return scaleY;
    }
   public void  setControlPoint(int x, int y)
   {
        controlPoint = new Point(x,y);
        notifyAllObservers();
   }
   public Point getControlPoint()
   {
       return controlPoint;
   }
   public void setTexture(BufferedImage image)
   {
       this.texture = image;
       textureChanged = true;
       notifyAllObservers();
   }
    public BufferedImage getTexture()
    {
        return texture;
    }
    public void notifyAllObservers()
    {
      for (AbstractImagePanel i: panels)
      {
          if(textureChanged)
          {
              i.textureChanged();
          }
          i.update();
      }
        textureChanged = false;
    }
    public void registerObserver(AbstractImagePanel panel)
    {
       panels.add(panel); 
    }
    boolean isSetControlPoint()
    {
        return controlPointSet;
    }
    public void setControlPoint(boolean mode)
    {
        controlPointSet = mode;
        notifyAllObservers();
    }
    void disableControlPoint()
    {
        controlPointSet = false;
    }
    public boolean isSetDraggedPoint()
    {
        return draggedPointSet;
    }
    public void setDraggedPointSet(boolean mode)
    {
        draggedPointSet = mode;
        notifyAllObservers();
    }
}
