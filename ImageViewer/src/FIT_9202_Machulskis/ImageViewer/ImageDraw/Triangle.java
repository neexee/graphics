package FIT_9202_Machulskis.ImageViewer.ImageDraw;

import java.awt.*;
import java.util.ArrayList;

/**
 * User: violetta
 * Date: 3/22/12
 * Time: 5:21 AM
 */
public class Triangle
{  ArrayList<Point> vertices;
   ArrayList<Point> currVertices;

    float startAngle;
    float endAngle;

    int numPixelsBorder;
    int numPixelsTotal;
    int numPixelsTransparent;

    Texture texture;
Triangle(Point first, Point second, Point third) {
    init();
    vertices = new ArrayList<Point>();
    currVertices = new ArrayList<Point>();
    vertices.add(first);
    vertices.add(second);
    vertices.add(third);
}

void init()
{
    startAngle = 0.0f;
    endAngle = 0.0f;
    numPixelsBorder = 0;
    numPixelsTotal = 0;
    numPixelsTransparent = 0;
}


ArrayList<Point> getCurrVertices()
{
    return currVertices;
}

Texture getTexture()
{
    return texture;
}


void setTexture(Texture texture) {
    this.texture = texture;
}

void setCurrVetices(Point v1, Point v2, Point v3)
{
    currVertices.clear();
    currVertices.add(v1);
    currVertices.add(v2);
    currVertices.add(v3);
}
public String toString()
{
    return vertices.get(0) +" "+ vertices.get(1) +" "+ vertices.get(2);
}
}
