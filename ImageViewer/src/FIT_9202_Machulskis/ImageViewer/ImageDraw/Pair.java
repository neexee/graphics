package FIT_9202_Machulskis.ImageViewer.ImageDraw;

/**
 * User: violetta
 * Date: 3/22/12
 * Time: 7:14 AM
 */
 public class Pair<T, S>
{
  public Pair(T f, S s)
  {
    first = f;
    second = s;
  }

  public T getFirst()
  {
    return first;
  }

  public S getSecond()
  {
    return second;
  }

  public String toString()
  {
    return "(" + first.toString() + ", " + second.toString() + ")";
  }

  protected T first;
  protected S second;
}
