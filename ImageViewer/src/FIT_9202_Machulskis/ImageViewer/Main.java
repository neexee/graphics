package FIT_9202_Machulskis.ImageViewer;
import javax.swing.*;
public
class Main
{


    public static void main(String[] args)
    {
                SwingUtilities.invokeLater(new Runnable()
                {
                    public
                    void run()
                    {
                         UIManager.put("swing.boldMetal", Boolean.FALSE);
                         MainWindowController controller = new MainWindowController();
                         controller.start();
                    }
                });
    }
}