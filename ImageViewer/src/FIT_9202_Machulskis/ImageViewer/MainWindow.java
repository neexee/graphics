package FIT_9202_Machulskis.ImageViewer;
import FIT_9202_Machulskis.ImageViewer.ImageDraw.AbstractImagePanel;
import FIT_9202_Machulskis.ImageViewer.ImageDraw.BilinearImagePanel;
import FIT_9202_Machulskis.ImageViewer.ImageDraw.NearestImagePanel;
import FIT_9202_Machulskis.ImageViewer.ImageDraw.TrilinearImagePanel;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class MainWindow extends JFrame
{
    final String NEAREST_PANEL_TITLE = "Image (Nearest)";
    final String BILINEAR_PANEL_TITLE = "Image (Bilinear)";
    final String TRILINEAR_PANEL_TITLE ="Image (Trilinear)";
    JMenuBar menuBar;
    JPanel mainPanel;
    ModePanel modePanel;
    NearestImagePanel nearestPanel;
    BilinearImagePanel bPanel;
    TrilinearImagePanel tPanel;
    JMenuItem openFile;
    ControlPanel control;
    JMenuItem exit; 
    final String MENU_NAME = "File";
    final String OPEN_FILE = "Open file ..";
    final String EXIT = "Exit";

    public MainWindow(String name)
    {
        super(name);
    }

    public void drawGUI()
    {

        mainPanel = new JPanel();    
        mainPanel.setLayout(new GridBagLayout());
        
        GridBagConstraints c = new GridBagConstraints();
        JPanel boxPanel = new JPanel();
        boxPanel.setLayout(new GridBagLayout());
        
        JPanel nearestPanelWrapper = new JPanel();
        nearestPanelWrapper.setLayout(new GridBagLayout());
        Border nearestPanelBorder = BorderFactory.createTitledBorder(NEAREST_PANEL_TITLE);
        nearestPanelWrapper.setBorder(nearestPanelBorder);
        nearestPanelWrapper.setPreferredSize(new Dimension(mainPanel.getWidth()/3, mainPanel.getHeight()));
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 1;
        nearestPanelWrapper.add(nearestPanel, c);

        boxPanel.add(nearestPanelWrapper, c);

        JPanel bPanelWrapper = new JPanel();
        bPanelWrapper.setLayout(new GridBagLayout());
        Border bPanelBorder = BorderFactory.createTitledBorder(BILINEAR_PANEL_TITLE);
        bPanelWrapper.setBorder(bPanelBorder);

        bPanelWrapper.setPreferredSize(new Dimension(mainPanel.getWidth()/3, mainPanel.getHeight()));

        bPanelWrapper.add(bPanel, c);
        c.gridx = 1;
        c.gridy = 0;
        boxPanel.add(bPanelWrapper, c);

        JPanel tPanelWrapper = new JPanel();
        tPanelWrapper.setLayout(new GridBagLayout());
        Border tPanelBorder = BorderFactory.createTitledBorder(TRILINEAR_PANEL_TITLE);
        tPanelWrapper.setBorder(tPanelBorder);

        tPanelWrapper.setPreferredSize(new Dimension(mainPanel.getWidth()/3, mainPanel.getHeight()));

        tPanelWrapper.add(tPanel, c);
        c.gridx = 2;
        c.gridy = 0;
        boxPanel.add(tPanelWrapper, c);
        c.gridx = 0;
        mainPanel.add(boxPanel, c);
        JPanel controlWrapper = new JPanel();
        controlWrapper.setLayout(new GridBagLayout());
        c.gridy = 0;
        c.gridx = 0;
        controlWrapper.add(control, c);

        c.gridx = 1;
        controlWrapper.add(modePanel, c);

        c.gridx = 0;
        c.gridy = 1;
        c.weighty =0;
        //c.gridwidth = 2;
        //c.weightx = 2;
        c.anchor= GridBagConstraints.SOUTHEAST;
        mainPanel.add(controlWrapper, c);
        menuBar = new JMenuBar();

        JMenu menu = new JMenu(MENU_NAME);
        openFile = new JMenuItem(OPEN_FILE);

        exit = new JMenuItem(EXIT);

        menu.add(openFile);
        menu.add(exit);
        menuBar.add(menu);
        setJMenuBar(menuBar);
        getContentPane().add(mainPanel);
        pack();

        setMinimumSize(new Dimension(control.getMinimumSize().width
                                     + modePanel.getMinimumSize().width
                                     + getInsets().left+ getInsets().right,

                                    control.getMinimumSize().height
                                    + getInsets().top
                                    + getInsets().bottom
                                    + menuBar.getHeight()
                            ));
        setPreferredSize(new Dimension(  control.getPreferredSize().width + modePanel.getPreferredSize().width,
                                         control.getPreferredSize().height +modePanel.getPreferredSize().height ));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
    public AbstractImagePanel getNearestPanel()
    {
        return nearestPanel;
    }
    public AbstractImagePanel getBilinearPanel()
    {
        return bPanel;
    }
    public void setNearestPanel(NearestImagePanel panel)
    {
        nearestPanel = panel;
    }
    public void setBilinearPanel(BilinearImagePanel panel)
    {
        bPanel = panel;
    }
    public void setModePanel(ModePanel panel)
    {
        modePanel = panel;
    }

    public void setControl(ControlPanel c)
    {
        control = c;
    }
    public JMenuItem getOpenFileMenuItem()
    {
      return openFile;
    }
    public JMenuItem getExitMenuItem()
    {
        return exit;
    }
    public ControlPanel getControl()
    {
        return control;
    }
    void setTrilinearPanel(TrilinearImagePanel p)
    {
       tPanel = p;
    }
}
