package FIT_9202_Machulskis.ImageViewer.Mode;

import FIT_9202_Machulskis.ImageViewer.ImageDraw.AbstractImagePanel;
import FIT_9202_Machulskis.ImageViewer.MainWindowController;

import java.awt.*;
import java.awt.event.MouseEvent;

public class MoveMode extends Mode
{
    Point lastPoint = new Point();

    public MoveMode(MainWindowController wc)
    {
        super(wc);
        wc.setControlPoint(false);
        wc.setDraggedPoint(false);
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent)
    {

    }

    @Override
    public void mousePressed(MouseEvent mouseEvent)
    {

        AbstractImagePanel nearestPanel = wc.getMainWindow().getNearestPanel();
        AbstractImagePanel bPanel =  wc.getMainWindow().getBilinearPanel();
        Point pressPoint = mouseEvent.getLocationOnScreen();
        if(nearestPanel.contains(pressPoint) || bPanel.contains(pressPoint))
        {
            System.out.println(pressPoint);
            lastPoint = pressPoint;
            wc.setMouseLastPressed(pressPoint);
        }

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent)
    {
        lastPoint = null;
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent)
    {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent)
    {

    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent)
    {
        Point p = mouseEvent.getLocationOnScreen();
        if(lastPoint!=null)
        {

            double deltaX = p.getX() - lastPoint.x;
            double deltaY = p.getY() - lastPoint.y;
            double positionX = wc.getModel().getPositionX();
            double positionY = wc.getModel().getPositionY();
            wc.getControlPanel().getPositionXSlider().setValue((int) (deltaX + positionX));
            wc.getControlPanel().getPositionYSlider().setValue((int) (deltaY + positionY));
        }
        lastPoint = p;
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent)
    {

    }
}
