package FIT_9202_Machulskis.ImageViewer.Mode;

import FIT_9202_Machulskis.ImageViewer.ImageDraw.AbstractImagePanel;
import FIT_9202_Machulskis.ImageViewer.MainWindowController;

import java.awt.*;
import java.awt.event.MouseEvent;

public class ScaleMode extends Mode
{
    private Point lastPressed;

    Point lastPoint = new Point();
    public ScaleMode(MainWindowController wc)
    {
        super(wc);
        wc.setControlPoint(true);
        wc.setDraggedPoint(true);
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent)
    {
        AbstractImagePanel nearestPanel =  wc.getMainWindow().getNearestPanel();
        AbstractImagePanel bPanel = wc.getMainWindow().getBilinearPanel();
        Point p = mouseEvent.getLocationOnScreen();
        Point pl ;
        if(mouseEvent.getButton() == MouseEvent.BUTTON3)
        {
            if(nearestPanel.contains(p))
            {

                pl = nearestPanel.getLocationOnScreen();
                wc.setControlPoint(new Point(p.x - pl.x, p.y - pl.y ));
            }
            else if(bPanel.contains(p))
            {
                pl = bPanel.getLocationOnScreen();
                wc.setControlPoint(new Point(p.x - pl.x, p.y - pl.y ));
            }

        }
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent)
    {

        AbstractImagePanel nearestPanel = wc.getMainWindow().getNearestPanel();
        AbstractImagePanel bPanel =  wc.getMainWindow().getBilinearPanel();
        Point pressPoint = mouseEvent.getLocationOnScreen();
        if(nearestPanel.contains(pressPoint) || bPanel.contains(pressPoint))
        {
            System.out.println(pressPoint);
            lastPoint = pressPoint;
            wc.setMouseLastPressed(pressPoint);
            wc.getModel().setDraggedPoint(pressPoint);
        }

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent)
    {
        lastPoint = null;
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent)
    {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent)
    {

    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent)
    {
        Point p = mouseEvent.getLocationOnScreen();
        if(lastPoint!=null)
        {

            double deltaX = p.getX() - lastPoint.x;
            double deltaY = p.getY() - lastPoint.y;
            Point control = wc.getModel().getControlPoint();
            //double positionY = wc.getModel().getPositionY();
            double scalex = 0;
          //  wc.getControlPanel().getPositionXSlider().setValue((int) (deltaX + positionX));
//            wc.getControlPanel().getPositionYSlider().setValue((int) (deltaY + positionY));
        }
        lastPoint = p;
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent)
    {

    }
}
