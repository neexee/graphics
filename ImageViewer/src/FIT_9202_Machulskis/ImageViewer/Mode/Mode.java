package FIT_9202_Machulskis.ImageViewer.Mode;

import FIT_9202_Machulskis.ImageViewer.MainWindowController;

import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public abstract class Mode implements  MouseMotionListener, MouseListener
{
    protected MainWindowController wc;
    Mode(MainWindowController wc)
    {
        this.wc = wc;
    }
}
