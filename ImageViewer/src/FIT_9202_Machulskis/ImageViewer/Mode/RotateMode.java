package FIT_9202_Machulskis.ImageViewer.Mode;

import FIT_9202_Machulskis.ImageViewer.ImageDraw.AbstractImagePanel;
import FIT_9202_Machulskis.ImageViewer.MainWindowController;

import java.awt.*;
import java.awt.event.MouseEvent;

public class RotateMode extends Mode
{
    private Point lastPressed;
    private double angle =0;
    public RotateMode(MainWindowController wc)
    {
        super(wc);
        wc.setControlPoint(true);
        wc.setDraggedPoint(false);
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent)
    {
        AbstractImagePanel nearestPanel =  wc.getMainWindow().getNearestPanel();
        AbstractImagePanel bPanel = wc.getMainWindow().getBilinearPanel();
        Point p = mouseEvent.getLocationOnScreen();
        Point pl ;
        if(mouseEvent.getButton() == MouseEvent.BUTTON3)
        {
            if(nearestPanel.contains(p))
            {

               pl = nearestPanel.getLocationOnScreen();
               wc.setControlPoint(new Point(p.x - pl.x, p.y - pl.y ));
            }
           else if(bPanel.contains(p))
            {
                 pl = bPanel.getLocationOnScreen();
                 wc.setControlPoint(new Point(p.x - pl.x, p.y - pl.y ));
            }
            angle =0;
        }
    }
    @Override
    public void mousePressed(MouseEvent mouseEvent)
    {
        AbstractImagePanel nearestPanel = wc.getMainWindow().getNearestPanel();
        AbstractImagePanel bPanel = wc.getMainWindow().getBilinearPanel();
        Point pressPoint = mouseEvent.getLocationOnScreen();
        if(nearestPanel.contains(pressPoint) || bPanel.contains(pressPoint))
        {
            lastPressed = pressPoint;
            wc.setMouseLastPressed(pressPoint);
        }
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent)
    {
        lastPressed = null;
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent)
    {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent)
    {

    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent)
    {
        Point p = mouseEvent.getLocationOnScreen();
        System.out.println("DRAG: " + p);
        if(lastPressed!=null)
        {   double deltaX1;
            double deltaY1;
            double deltaX2;
            double deltaY2;
            AbstractImagePanel panel = null;
            if(wc.getMainWindow().getNearestPanel().hasPoint(p))
            {
                panel = wc.getMainWindow().getNearestPanel();
            }
            else if(wc.getMainWindow().getBilinearPanel().hasPoint(p))
            {
                panel = wc.getMainWindow().getBilinearPanel();
            }
            else
            {
                return;
            }
            p = new Point((int)(p.getX() - panel.getLocationOnScreen().getX()),(int) (p.getY() - panel.getLocationOnScreen().getY()));
            deltaX1 = p.getX()- - wc.getControlPoint().getX();
            deltaY1 = p.getY() - wc.getControlPoint().y;
            deltaX2 = lastPressed.x - wc.getControlPoint().x;
            deltaY2 = lastPressed.y - wc.getControlPoint().y;

            double cosFi = (deltaX1*deltaX2 + deltaY1*deltaY2)/(Math.sqrt((deltaX1*deltaX1  + deltaY1*deltaY1 )*(deltaX2*deltaX2 + deltaY2*deltaY2)));
            double arcCosFi = Math.acos(cosFi);
            double deltaX = p.getX() - lastPressed.x;
            double deltaY = p.getY() - lastPressed.y;
            
            if(deltaY*deltaX2 - deltaX*deltaY2 > 0)
            {
                arcCosFi = -arcCosFi;
            }
            angle+=arcCosFi*Math.PI;
            wc.getControlPanel().getAngleSlider().setValue((int)angle);
        }
        lastPressed = p;

    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent)
    {

    }
}
