package FIT_9202_Machulskis.ImageViewer;

import FIT_9202_Machulskis.ImageViewer.Mode.MoveMode;
import FIT_9202_Machulskis.ImageViewer.Mode.RotateMode;
import FIT_9202_Machulskis.ImageViewer.Mode.ScaleMode;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ModePanel extends JPanel  implements ActionListener
{
    private final String PANEL_TITLE = "Mode";
    private final String MOVE_LABEL = "Move";
    private final String SCALE_LABEL = "Scale";
    private final String ROTATE_LABEL = "Rotate";
    JRadioButton rotateButton;
    JRadioButton moveButton;
    JRadioButton scaleButton;
    ButtonGroup buttonGroup;
    MainWindowController wc;

    ModePanel(MainWindowController wc)
            
    {
        GridBagConstraints c = new GridBagConstraints();

        Border modeBorder = BorderFactory.createTitledBorder(PANEL_TITLE);
        setBorder(modeBorder);
        setLayout(new GridBagLayout());
        moveButton = new JRadioButton(MOVE_LABEL);
        rotateButton = new JRadioButton(ROTATE_LABEL);
        scaleButton = new JRadioButton(SCALE_LABEL);


        buttonGroup = new ButtonGroup();

        buttonGroup.add(moveButton);
        buttonGroup.add(rotateButton);
        buttonGroup.add(scaleButton);

        moveButton.setSelected(true);

        moveButton.addActionListener(this);
        moveButton.setActionCommand(MOVE_LABEL);
        rotateButton.addActionListener(this);
        rotateButton.setActionCommand(ROTATE_LABEL);
        scaleButton.addActionListener(this);
        scaleButton.setActionCommand(SCALE_LABEL);
        
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        add(moveButton, c);
        c.gridy = 1;
        add(rotateButton, c);
        c.gridy = 2;
        add(scaleButton, c);

        setMinimumSize(getPreferredSize());
        this.wc = wc;
    }


    @Override
    public void actionPerformed(ActionEvent actionEvent)
    {
        String actionStr = actionEvent.getActionCommand();
        if(actionStr.equals(MOVE_LABEL))
        {
            wc.setMode(new MoveMode(wc));
        }
        else if(actionStr.equals(ROTATE_LABEL))
        {
           wc.setMode(new RotateMode(wc));
        }
        else
        {
            wc.setMode(new ScaleMode(wc));
        }
        
    }
}
