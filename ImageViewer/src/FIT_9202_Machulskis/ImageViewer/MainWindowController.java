package FIT_9202_Machulskis.ImageViewer;

import FIT_9202_Machulskis.ImageViewer.ImageDraw.BilinearImagePanel;
import FIT_9202_Machulskis.ImageViewer.ImageDraw.ImageModel;
import FIT_9202_Machulskis.ImageViewer.ImageDraw.NearestImagePanel;
import FIT_9202_Machulskis.ImageViewer.ImageDraw.TrilinearImagePanel;
import FIT_9202_Machulskis.ImageViewer.Mode.Mode;
import FIT_9202_Machulskis.ImageViewer.Mode.MoveMode;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * User: violetta
 * Date: 3/12/12
 * Time: 2:03 AM
 */
public class MainWindowController
{

    private MainWindow mainWindow;
    private JFileChooser fc;
    private Point mouseLastPressed;
    private Mode mode;
    private Point controlPoint;
    private ImageModel model;

    public MainWindowController()
    {
          mainWindow = new MainWindow("ImageViewer");
          model = new ImageModel();
          controlPoint = new Point();
          setMode(new MoveMode(this));

    }

    public void setMode(Mode _mode)
    {
        if(mode !=null)
        {
            mainWindow.removeMouseListener(mode);
            mainWindow.removeMouseMotionListener(mode);
        }
        mode = _mode;
        mainWindow.addMouseListener(mode);
        mainWindow.addMouseMotionListener(mode);
    }
    public Point getControlPoint()
    {
        return controlPoint;
    }
    public void setControlPoint(Point p)
    {
        controlPoint = p;
        model.setControlPoint(p.x, p.y);
    }
    public MainWindow getMainWindow()
    {
        return mainWindow;
    }
    public Point getMouseLastPressed()
    {
        return mouseLastPressed;
    }
    public void setMouseLastPressed(Point p)
    {
       mouseLastPressed = p;
    }
    public void start()
    {

        mainWindow.setBilinearPanel(new BilinearImagePanel(model));
        mainWindow.setNearestPanel(new NearestImagePanel(model));
        mainWindow.setTrilinearPanel(new TrilinearImagePanel(model));
        mainWindow.setControl(new ControlPanel(model));
        mainWindow.setModePanel(new ModePanel(this));
        fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        mainWindow.drawGUI();
        JMenuItem openFile = mainWindow.getOpenFileMenuItem();
        openFile.addActionListener(new ActionListener()
        {
            @Override
            public
            void actionPerformed(ActionEvent e)
            {
                int returnVal = fc.showOpenDialog(mainWindow);

                if(returnVal==JFileChooser.APPROVE_OPTION)
                {
                    File file = fc.getSelectedFile();
                    BufferedImage img;
                    try
                    {
                        img = ImageIO.read(file);
                        model.setTexture(img);
                    }
                    catch(IOException e1)
                    {
                        JOptionPane.showMessageDialog(mainWindow, e1.getLocalizedMessage());
                    }

                }
            }
        });
        JMenuItem exit = mainWindow.getExitMenuItem();
        exit.addActionListener(new ActionListener()
        {
            @Override
            public
            void actionPerformed(ActionEvent e)
            {
                mainWindow.dispose();
            }
        });

    }
    public ImageModel getModel()
    {
        return model;
    }
    public ControlPanel getControlPanel()
    {
        return mainWindow.getControl();
    }
    public void setControlPoint(boolean mode)
    {
        model.setControlPoint(mode);
    }
    public void setDraggedPoint(boolean mode)
    {
        model.setDraggedPointSet(mode);
    }
}