package FIT_9202_Machulskis.Plotter;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class FocusPickListener implements MouseMotionListener, MouseListener
{
    private final int PICK_RADIUS = 50;
    private final int PICK_RADIUS_2 = PICK_RADIUS*PICK_RADIUS;  // 20^2
    Point movePoint = new Point();
    boolean ffmove = false;
    int deltax = 0;
    int deltay = 0;
    boolean sfmove = false;
    ControlPanel control;
    LemniscatePanel panel;
    
    public FocusPickListener(ControlPanel panel)
    {
       this.panel = panel.getLemniscatePanel();
       control = panel;
    }
    @Override
    public void mouseClicked(MouseEvent mouseEvent)
    {

    }

    @Override
    public void mousePressed(MouseEvent mouseEvent)
    {
        Point pressPoint = mouseEvent.getLocationOnScreen();
        Point ko = panel.getLocationOnScreen();
        pressPoint.x = pressPoint.x -ko.x;
        pressPoint.y = pressPoint.y -ko.y;

        Point ff = panel.getFirstFocus();
        Point sf = panel.getSecondFocus();
        int deltax1 = ff.x + panel.width/2-  pressPoint.x;
        int deltay1 = ff.y  + panel.height/2- pressPoint.y;

        int deltax2 = sf.x + panel.width/2- pressPoint.x;
        int deltay2 = sf.y + panel.height/2 - pressPoint.y;
        int r1 = deltax1*deltax1 + deltay1*deltay1;
        int r2 = deltax2*deltax2 + deltay2*deltay2;
        if(r1 < PICK_RADIUS_2 && r1 < r2)
        {
            movePoint = pressPoint;
            ffmove = true;
            deltax = deltax1;
            deltay = deltay1;
        }
        else if(r2< PICK_RADIUS_2)
        {
            movePoint = pressPoint;
            sfmove = true;
            deltax = deltax2;
            deltay = deltay2;
        }
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent)
    {
        ffmove = false;
        sfmove = false;
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent)
    {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent)
    {

    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent)
    {
      Point dragPoint = mouseEvent.getLocationOnScreen();
      Point ko = panel.getLocationOnScreen();
      dragPoint.x = dragPoint.x - panel.width/2 - ko.x + deltax;
      dragPoint.y = dragPoint.y - panel.height/2 - ko.y + deltay;
      if(ffmove)
      {
          control.setFirstFocus(dragPoint);
          panel.setFirstFocus(dragPoint);

      }
      if(sfmove)
      {
          control.setSecondFocus(dragPoint);
          panel.setSecondFocus(dragPoint);

      }
      
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent)
    {

    }
}
