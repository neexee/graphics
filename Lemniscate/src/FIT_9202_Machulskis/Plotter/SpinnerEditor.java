package FIT_9202_Machulskis.Plotter;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

abstract public class SpinnerEditor extends JSpinner.DefaultEditor
{
    private final int SPINNER_WIDTH = 30;
    private final int SPINNER_HEIGHT = 30;
    private final int DELTA_T = 1000;
    JSpinner spinner;
    JSlider slider;
    String lastString = "";
    protected boolean flag = true;

    public SpinnerEditor(final JSpinner spinner)
    {
        super(spinner);
        this.setOpaque(true);
        getTextField().setEditable(true);
        getTextField().setFocusLostBehavior(JFormattedTextField.COMMIT);
        getTextField().addFocusListener(new FocusListener()
        {
            @Override
            public void focusGained(FocusEvent focusEvent)
            {
            }

            @Override
            public void focusLost(FocusEvent focusEvent)
            {
                FormattedTextFieldVerifier fv = new FormattedTextFieldVerifier();
                boolean check = fv.verify(SpinnerEditor.this.getTextField());
                if(!check)
                {
                    SpinnerEditor.this.getTextField().requestFocus();
                    SpinnerEditor.this.getTextField().setText(lastString);
                    showTip();
                }

            }
        });
        ControlModel model = (ControlModel) spinner.getModel();
        spinner.setToolTipText("Please type number between " + model.getMin() + " and " + model.getMax());

        getTextField().addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyTyped(KeyEvent e)
            {
                if(e.getKeyCode()==KeyEvent.VK_ENTER)
                {

                    commitEdit();
                }
                else
                {
                    JFormattedTextField format = getTextField();
                    FormattedTextFieldVerifier fv = new FormattedTextFieldVerifier();
                    boolean check = fv.verify(format);
                    if(!check)
                    {
                        lastString = format.getText();
                        showTip(); //format.setText("");
                    }
                }
            }
        });
        Dimension size = new Dimension(SPINNER_WIDTH, SPINNER_HEIGHT);
        setMinimumSize(size);
        setPreferredSize(size);
        this.spinner = spinner;

        spinner.addChangeListener(this);

    }

    private void showTip()
    {
        PopupFactory popupFactory = PopupFactory.getSharedInstance();
        int x = spinner.getLocationOnScreen().x;
        int y = spinner.getLocationOnScreen().y;
        x += spinner.getWidth() / 2;
        y += spinner.getHeight();
        JToolTip tip = spinner.createToolTip();
        tip.setTipText(spinner.getToolTipText());
        final Popup tooltipContainer = popupFactory.getPopup(spinner, tip, x, y);
        tooltipContainer.show();
        (new Thread(new Runnable()
        {
            public void run()
            {
                try
                {
                    Thread.sleep(DELTA_T);
                }
                catch(InterruptedException ex)
                {

                }
                tooltipContainer.hide();
            }

        })).start();
    }

    public void addSliderListener(JSlider slider)
    {
        this.slider = slider;
    }

    public void updateValue(double value)
    {
        flag = false;
        spinner.setValue(value);
    }

    public void updateValue(JSpinner spinner)
    {
        {
            Double value = (Double) spinner.getValue();
            if(flag)
            {
               makeSomethingWithPane(value.doubleValue());
            }
            flag = true;
            
            spinner.setValue(value.doubleValue());
            slider.setValue(value.intValue());
        }
    }

    @Override
    public void commitEdit()
    {
        JFormattedTextField format = getTextField();
        FormattedTextFieldVerifier fv = new FormattedTextFieldVerifier();
        boolean check = fv.verify(format);

        if(check==true)
        {
            updateValue(spinner);
        }
        else
        {
            try
            {
                spinner.setValue(spinner.getPreviousValue());
            }
            catch(IllegalArgumentException ex)
            {
            }
        }
    }

    public abstract void makeSomethingWithPane(double value);

    @Override
    public void stateChanged(ChangeEvent e)
    {
        super.stateChanged(e);
        updateValue((JSpinner) e.getSource());

    }
}
