package FIT_9202_Machulskis.Plotter;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Slider extends JSlider
{
    public Slider(final JSpinner spinner, double minimum, double maximum, double value)
    {
        super((int)minimum, (int)maximum);
        setValue((int)value);
        this.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                JSlider slider = (JSlider) (e.getSource());
                    SpinnerEditor spinnerEditor = (SpinnerEditor) spinner.getEditor();
                    spinnerEditor.updateValue(slider.getValue());
            }

        });

    }
}

