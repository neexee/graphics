package FIT_9202_Machulskis.Plotter;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.io.*;
import java.util.Properties;
public
class ControlPanel extends JPanel
{
    Properties properties;

    LemniscatePanel panel;
    JSpinner X1Spinner;
    JSpinner X2Spinner;
    JSpinner Y1Spinner;
    JSpinner Y2Spinner;

    JSlider Y2Slider;
    JSlider Y1Slider;
    JSlider X2Slider;
    JSlider X1Slider;
    JSpinner RSpinner;

    private final String R_LABEL = "R";

    private final String X1_LABEL = "X1";
    private final String X2_LABEL = "X2";

    private final String Y1_LABEL = "Y1";
    private final String Y2_LABEL = "Y2";

    ControlPanel(final LemniscatePanel pane)
    {
        this.panel = pane;
        properties = new Properties();
        GridBagConstraints c = new GridBagConstraints();
        final int SPINNER_STEP = 1;
        final String PANEL_TITLE = "Control";
        Border controlBorder = BorderFactory.createTitledBorder(PANEL_TITLE);
        setBorder(controlBorder);
        setLayout(new GridBagLayout());
/*X1 spinner/slider */
        c.gridx = 0;
        c.gridy = 0;
        JLabel x1Label = new JLabel(X1_LABEL);
        add(x1Label, c);

        c.gridx = 0;
        c.gridy = 1;


        X1Spinner = new JSpinner(new ControlModel(LemniscatePanel.MIN_X,
                LemniscatePanel.MAX_X, LemniscatePanel.DEFAULT_X1, SPINNER_STEP));

        SpinnerEditor sp = new SpinnerEditor(X1Spinner)
        {
            @Override
            public void makeSomethingWithPane(double value)
            {
                Point p = new Point(pane.getFirstFocus());
                p.x = (int)(value);
                pane.setFirstFocus(p);
            }
        };

        X1Spinner.setEditor(sp);
        add(X1Spinner, c);

        X1Slider = new Slider(X1Spinner, LemniscatePanel.MIN_X, LemniscatePanel.MAX_X, LemniscatePanel.DEFAULT_X1);
        sp.addSliderListener(X1Slider);
        c.gridx = 0;
        c.gridy = 2;
        add(X1Slider, c);

/*Y1 spinner/slider */
        c.gridx = 1;
        c.gridy = 0;
        JLabel y1Label = new JLabel(Y1_LABEL);
        add(y1Label, c);

        c.gridx = 1;
        c.gridy = 1;

        Y1Spinner = new JSpinner(new ControlModel(LemniscatePanel.MIN_X,
                LemniscatePanel.MAX_X, LemniscatePanel.DEFAULT_Y1, SPINNER_STEP));

        SpinnerEditor spedy1 = new SpinnerEditor(Y1Spinner)
        {
            @Override
            public void makeSomethingWithPane(double value)
            {
                Point p = new Point(pane.getFirstFocus());
                p.y = -(int)(value);
                pane.setFirstFocus(p);
            }
        };

        Y1Spinner.setEditor(spedy1);
        add(Y1Spinner, c);

        Y1Slider = new Slider(Y1Spinner, LemniscatePanel.MIN_X, LemniscatePanel.MAX_X, LemniscatePanel.DEFAULT_Y1);
        spedy1.addSliderListener(Y1Slider);
        c.gridx = 1;
        c.gridy = 2;
        add(Y1Slider, c);

/*X2 spinner/slider */
        c.gridx = 2;
        c.gridy = 0;
        JLabel x2Label = new JLabel(X2_LABEL);
        add(x2Label, c);

        c.gridx = 2;
        c.gridy = 1;

        X2Spinner = new JSpinner(new ControlModel(LemniscatePanel.MIN_X,
                LemniscatePanel.MAX_X, LemniscatePanel.DEFAULT_X2, SPINNER_STEP));

        SpinnerEditor spx2 = new SpinnerEditor(X2Spinner)
        {
            @Override
            public void makeSomethingWithPane(double value)
            {
                Point p;
                if( pane.getSecondFocus() == null)
                {
                    p = new Point();
                }
                else
                {            
                    p =new Point( pane.getSecondFocus());
                }
                p.x = (int)(value);
                pane.setSecondFocus(p);
            }
        };

        X2Spinner.setEditor(spx2);
        add(X2Spinner, c);

        X2Slider = new Slider(X2Spinner, LemniscatePanel.MIN_X, LemniscatePanel.MAX_X, LemniscatePanel.DEFAULT_X2);
        spx2.addSliderListener(X2Slider);
        c.gridx = 2;
        c.gridy = 2;
        add(X2Slider, c);

 /*Y2 spinner/slider */
        c.gridx = 3;
        c.gridy = 0;
        JLabel y2Label = new JLabel(Y2_LABEL);
        add(y2Label, c);

        c.gridx = 3;
        c.gridy = 1;

        Y2Spinner = new JSpinner(new ControlModel(LemniscatePanel.MIN_X,
                LemniscatePanel.MAX_X, LemniscatePanel.DEFAULT_Y2, SPINNER_STEP));

        SpinnerEditor spedy2 = new SpinnerEditor(Y2Spinner)
        {
            @Override
            public void makeSomethingWithPane(double value)
            {
                Point p = new Point(pane.getSecondFocus());
                p.y =- (int)(value);
                pane.setSecondFocus(p);
            }
        };

        Y2Spinner.setEditor(spedy2);
        add(Y2Spinner, c);


        Y2Slider = new Slider(Y2Spinner, LemniscatePanel.MIN_X, LemniscatePanel.MAX_X, LemniscatePanel.DEFAULT_Y2);
        spedy2.addSliderListener(Y2Slider);
        c.gridx = 3;
        c.gridy = 2;
        add(Y2Slider, c);

/*R spinner/slider */
        c.gridx = 4;
        c.gridy = 0;
        JLabel phaseLabel = new JLabel(R_LABEL);
        add(phaseLabel, c);

        c.gridx = 4;
        c.gridy = 1;

        RSpinner = new JSpinner(new ControlModel(LemniscatePanel.MIN_R,

                                                     LemniscatePanel.MAX_R, LemniscatePanel.DEFAULT_R, SPINNER_STEP));

        SpinnerEditor sped = new SpinnerEditor(RSpinner)
        {
            @Override
            public void makeSomethingWithPane(double value)
            {
                pane.setR(value);
            }
        };

        RSpinner.setEditor(sped);
        add(RSpinner, c);

        JSlider RSlider = new Slider(RSpinner, LemniscatePanel.MIN_R, LemniscatePanel.MAX_R, LemniscatePanel.DEFAULT_R);
        sped.addSliderListener(RSlider);
        c.gridx = 4;
        c.gridy = 2;
        add(RSlider, c);
        Dimension size = new Dimension(getPreferredSize());
        setMinimumSize(size);


    }
   LemniscatePanel getLemniscatePanel()
   {
       return panel;
   }
   void setFirstFocus(Point p)
   {
       
       X1Slider.setValue(p.x);
       Y1Slider.setValue(-p.y);
   }
    void setSecondFocus(Point p)
    {
        X2Slider.setValue(p.x);
        Y2Slider.setValue(-p.y);
    }
  public
    void savePropetries(File file) throws IOException
    {
        FileOutputStream stream = new FileOutputStream(file);
        properties = new Properties();
        properties.setProperty(X1_LABEL, X1Spinner.getValue().toString());
        properties.setProperty(X2_LABEL, X2Spinner.getValue().toString());
        properties.setProperty(Y1_LABEL, Y1Spinner.getValue().toString());
        properties.setProperty(Y2_LABEL, Y2Spinner.getValue().toString());

        final String CONFIG_COMMENT = "Plotter";
        properties.storeToXML(stream, CONFIG_COMMENT);
        stream.close();

    }

    public
    void applyConfig(File file) throws IOException
    {
        FileInputStream stream;
        try
        {
            stream = new FileInputStream(file);
        }
        catch(FileNotFoundException e)
        {
            e.printStackTrace();
        }


        stream = new FileInputStream(file);
        properties.loadFromXML(stream);
        String value;
        try
        {
            value = properties.getProperty(X1_LABEL, Double.toString(LemniscatePanel.DEFAULT_X1));
            X1Spinner.setValue(Double.parseDouble(value));
        }
        catch (NumberFormatException ex)
        {
           X1Spinner.setValue(LemniscatePanel.DEFAULT_X1);
        }
        try
        {
            value = properties.getProperty(X2_LABEL, Double.toString(LemniscatePanel.DEFAULT_X2));
            X2Spinner.setValue(Double.parseDouble(value));
        }
        catch (NumberFormatException ex)
        {
             X2Spinner.setValue(LemniscatePanel.DEFAULT_X2);
        }
        try
        {
             value = properties.getProperty(Y1_LABEL, Double.toString(LemniscatePanel.DEFAULT_Y1));
             Y1Spinner.setValue(Double.parseDouble(value));
        }
        catch (NumberFormatException ex)
        {
            Y1Spinner.setValue(LemniscatePanel.DEFAULT_Y1);
        }
        try
        {
             value = properties.getProperty(Y2_LABEL, Double.toString(LemniscatePanel.DEFAULT_Y2));
             Y2Spinner.setValue(Double.parseDouble(value));
        }
        catch (NumberFormatException ex)
        {
            Y2Spinner.setValue(LemniscatePanel.DEFAULT_Y2);
        }
        
        try
        {
             value = properties.getProperty(R_LABEL, Double.toString(LemniscatePanel.DEFAULT_R));
             RSpinner.setValue(Double.parseDouble(value));
        }
        catch (NumberFormatException ex)
        {
            RSpinner.setValue(LemniscatePanel.DEFAULT_R);
        }
      stream.close();
    }
}
